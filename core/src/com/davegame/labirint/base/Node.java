package com.davegame.labirint.base;

/**
 * Created by Dave on 09/11/2019.
 */

public class Node {
    public final int x;
    public final int y;


    Node(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
