package com.davegame.labirint.base;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.davegame.labirint.base.mazegenerator.GameAssets;
import com.davegame.labirint.objects.DaveGame;
import com.davegame.labirint.objects.PlayerData;

/**
 * Created by Dave on 08/09/2019.
 */

public abstract class BaseScreen implements Screen,InputProcessor{
    protected DaveGame game;
   // public final static int viewWidth = Gdx.graphics.getWidth();
    //public final static int viewHeight = Gdx.graphics.getHeight();
    public static String gameTag = "Labirint";

    public static boolean paused;

    public  Stage mainStage, uiStage;
    private Camera camera;

    public final static int viewWidth = 1080;
    public final static int viewHeight =1920;

    public static boolean sounOn = true;





   /* public void loadCompanyLogo(){
        if(gameAssets == null){
            gameAssets = new GameAssets();
            gameAssets.loadCompanyLogo();
            gameAssets.manager.finishLoading();
        }

    }

    public void loadGameData(){


            gameAssets.loadTextures();
            gameAssets.manager.finishLoading();


    }*/


   public void switchSound(){
       sounOn = !sounOn;
       PlayerData.saveSound(sounOn);
   }


    @Override
    public void show() {
    }

    public Camera returnCamera(){
        return  camera;
    }

    @Override
    public void render(float delta) {
        uiStage.act();
        if(!paused){
            mainStage.act(delta);
            update(delta);
        }

        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mainStage.draw();
        uiStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        mainStage.getViewport().update(width,height,true);
        uiStage.getViewport().update(width,height,true);
        //camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(),0));
    }

    public void setCameraPosition(float x, float y){
        camera.position.x = x;
        camera.position.y =y;
    }
    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    static {

        if(PlayerData.getFitstTime() == true){
            sounOn = PlayerData.loadSoundBoolean();
        }else{
            sounOn = true;
        }
    }

    public BaseScreen(DaveGame g){

        mainStage = new Stage(new FitViewport(viewWidth,viewHeight));
        uiStage = new Stage (new FitViewport(viewWidth,viewHeight));


        game = g;
        InputMultiplexer im = new InputMultiplexer(this, mainStage, uiStage);
        Gdx.input.setInputProcessor(im);
        camera = new OrthographicCamera(viewWidth, viewHeight);


        create();
    }

    public static void togglePause(){
        paused = !paused;

    }

    public abstract void create();

    public abstract void update(float delta);
    static{

        paused =false;

    }

    public Vector3 unprojectWithCamera(Vector3 toUnproject){

        Vector3 unProjected = uiStage.getViewport().unproject(toUnproject);//camera.unproject(toUnproject);


        return unProjected;

    }


    public static void writeLog(String logText){
        Gdx.app.log(gameTag, logText);

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
