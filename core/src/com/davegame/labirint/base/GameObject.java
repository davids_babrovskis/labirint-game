package com.davegame.labirint.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;


/**
 * Created by Dave on 08/09/2019.
 */

public class GameObject extends Actor {
    public TextureRegion region;
    public Rectangle textureBoundary;


    private TextureRegion[] txTegion;
    private Animation anim;
    public Polygon boundingPolygon;

    private ShapeRenderer shpRander;
    public boolean enableDebug;

    public Circle boundingCircle;

    private int spriteRotation=0;


    public int getSpriteRotation(){return spriteRotation;}

    public void setSpriteRotation(int spriteRotation){this.spriteRotation = spriteRotation;}


    public GameObject(){

        super();
        region = new TextureRegion();
        boundingPolygon = null;
        shpRander = new ShapeRenderer();

    }


    public void setTextureRegion(TextureRegion txReg){
        region = txReg;
        int w = txReg.getRegionWidth();
        int h = txReg.getRegionHeight();

        super.setWidth(w);
        super.setHeight(h);

        enableDebug = false;

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color c =getColor();
        batch.setColor(c.r,c.g,c.b, c.a);

        if(super.isVisible()){
            batch.draw(region, super.getX(), super.getY(), super.getOriginX(), super.getOriginY(),getWidth(), getHeight(), getScaleX(), getScaleY(),getRotation());
        }
        if(enableDebug){
            drawDebug(shpRander);

        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }
    public void setBoundingRectangle(){
        float w = getWidth();
        float h = getHeight();
        float [] verticles = {0,0,w,0,w,h,0,h};
        boundingPolygon = new Polygon(verticles);
        boundingPolygon.setOrigin(getOriginX(), getOriginY());
    }

    public void setBoundingCircle(){
        boundingCircle = new Circle();
        boundingCircle.set(this.getX() , this.getY(), (getWidth() /2) -5);


    }

    public Circle getBoundingCircle(float posX, float posY){

        boundingCircle.setPosition(posX + this.getWidth()/2, posY + this.getWidth()/2);
        return boundingCircle;

    }

    public Rectangle getTextureBoundary(){
        textureBoundary.setPosition(this.getX(), this.getY());
        return textureBoundary;
    }


    public void disposeActor(){
        this.addAction(Actions.removeActor());
    }



    public void setEllipseBoundary(){
        int n=8;
        float w = getWidth();
        float h = getHeight();

        float [] verticles = new float[2*n];
        for(int i=0; i<n; i++){
            float t = i* 6.28f /n;

            //x-coordinates
            verticles[2*i] = w/2 * MathUtils.cos(t) + w/2;

            //y-coordinates
            verticles[2*i+1] = h/2 * MathUtils.sin(t) + h/2;


        }

        boundingPolygon = new Polygon(verticles);
        boundingPolygon.setOrigin(getOriginX(), getOriginY());
    }

    public Polygon getBoundingPolygon(){
        boundingPolygon.setPosition(getX(), getY());
        boundingPolygon.setRotation(getRotation());
        return boundingPolygon;

    }

    public boolean overlaps(GameObject other, boolean resolve){
        Polygon poly1 = this.getBoundingPolygon();
        //if(other != null)
        Polygon poly2 = other.getBoundingPolygon();

        if(!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())){
            return false;
        }

        Intersector.MinimumTranslationVector mtv = new Intersector.MinimumTranslationVector();
        boolean polyOverlap = Intersector.overlapConvexPolygons(poly1, poly2, mtv);
        if(polyOverlap && resolve){
            this.moveBy(mtv.normal.x * mtv.depth, mtv.normal.y * mtv.depth);
        }
        float significant = 0.5f;
        return (polyOverlap && (mtv.depth > significant));

    }

    public void copy(GameObject origin){
        this.region = new TextureRegion(origin.region);
        if(origin.boundingPolygon != null){
            this.boundingPolygon = new Polygon(origin.boundingPolygon.getVertices());
            this.boundingPolygon.setOrigin(origin.getOriginX(), origin.getOriginY());

        }

        this.setPosition(origin.getX(), origin.getY());
        this.setOriginX(origin.getOriginX());
        this.setOriginY(origin.getOriginY());
        this.setWidth(origin.getWidth());
        this.setHeight(origin.getHeight());
        this.setColor(origin.getColor());
        this.setVisible(origin.isVisible());
    }

    public GameObject clone(){
        GameObject newGameObject = new GameObject();
        newGameObject.copy(this);
        return newGameObject;
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        shapes.begin(ShapeRenderer.ShapeType.Line);
        //shapes.polygon(boundingPolygon.getTransformedVertices());

        if(boundingCircle != null){
            shapes.circle(boundingCircle.x,boundingCircle.y,boundingCircle.radius);
        }
        if(textureBoundary != null){
            shapes.rect(textureBoundary.getX(), textureBoundary.getY(), textureBoundary.getWidth(), textureBoundary.getHeight());
        }
        shapes.end();

        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glDisable(GL20.GL_BLEND);

    }

}
