package com.davegame.labirint.base.mazegenerator;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Dave on 11/02/2020.
 */

public class GameAssets
{
    public final String gamePack  = "pack";
    public final String helpScreenLoc = "helpScreen.png";

    public final String font1Location = "fonts/gamefont.fnt";
    public final String font2Location = "fonts/gamefont2.fnt";
    public final String font3Location = "fonts/LevelComplete.fnt";
    public final String font4Location = "fonts/GameOver.fnt";
    public final String font5Location = "fonts/LevelName.fnt";

    public final String titleImageLocation = "titleImg.png";
    public final String bgImageLocation = "bgMaze.png";
    public final String darkBgLocation ="darkBg.png";
    public final String shopBg ="shopScr.png";
    public final String bestScImg = "bestScor.png";

    public final String mainThemeLoc = "music/1.mp3";
    public final String loop1 = "music/2.mp3";
    public final String loop2 = "music/3.mp3";
    public final String loop3 = "music/4.mp3";


    public final String buttonClick = "sound/tik.mp3";
    public final String levelComplete = "sound/levelComplete.mp3";
    public final String coinTaken = "sound/coin.mp3";
    public final String gameOver = "sound/gameOver.mp3";
    public final String torchSound = "sound/torch.mp3";
    public final String scarySound = "sound/scarySf.mp3";


    public final String ghostSound = "sound/ghost.mp3";
    public final String shieldSound = "sound/shield.mp3";

    public final String soundOnImg = "soundOnBt.png";
    public final String soundOfImg = "soundOfBt.png";



    public final String taskText = "taskText.png";



    public final AssetManager manager = new AssetManager();



    public void loadTaskText(){
        manager.load(taskText, Texture.class);

    }

    public Texture getTaskText(){
        return manager.get(taskText);

    }

    public void loadTextures(){
        manager.load(gamePack, TextureAtlas.class);

    }

    public void loadSoundBtIm(){
        manager.load(soundOnImg, Texture.class);
        manager.load(soundOfImg, Texture.class);
    }

    public Texture getSoundOfImg(){
        return  manager.get(soundOfImg);
    }

    public  Texture getSoundOnImg(){
        return  manager.get(soundOnImg);
    }

    public void loadSound(){
        manager.load(buttonClick, Sound.class);
        manager.load(levelComplete, Sound.class);
        manager.load(coinTaken, Sound.class);
        manager.load(gameOver, Sound.class);
        manager.load(torchSound, Sound.class);
        manager.load(ghostSound, Sound.class);
        manager.load(shieldSound, Sound.class);
        manager.load(scarySound, Sound.class);

    }

    public Sound getScarySound(){return  manager.get(scarySound);}

    public Sound getShieldSound(){
        return manager.get(shieldSound);
    }

    public Sound getGhostSound(){
        return manager.get(ghostSound);
    }


    public Sound getTorchSound(){
        return manager.get(torchSound);
    }

    public Sound getGameOverSound(){
        return  manager.get(gameOver);
    }

    public Sound getLevelCpSound(){
        return  manager.get(levelComplete);
    }
    public  Sound getCoinTaken(){
        return  manager.get(coinTaken);
    }

    public void loadMusic(){
        manager.load(mainThemeLoc, Music.class);
        manager.load(loop1, Music.class);
        manager.load(loop2, Music.class);
        manager.load(loop3, Music.class);
    }

    public Music getMainTheme(){

        return manager.get(mainThemeLoc);
    }

    public Music getLoop1(){
        return manager.get(loop1);

    }
    public Music getLoop2(){
        return manager.get(loop2);

    }
    public Music getLoop3(){
        return manager.get(loop3);

    }


    public Sound getButtonClick(){
        return  manager.get(buttonClick, Sound.class);
    }



    public TextureAtlas getTextureAtlas(){
        return  manager.get(gamePack);

    }

    public void loadBestImg(){
        manager.load(bestScImg, Texture.class);
    }
    public Texture getBestImg(){
        return  manager.get(bestScImg);
    }


    public void loadHelpScreen(){
        manager.load(helpScreenLoc, Texture.class);
    }
    public Texture getHelpScreen(){
        return  manager.get(helpScreenLoc);
    }

    public void loadDarkBGScreen(){
        manager.load(darkBgLocation, Texture.class);
    }
    public Texture getDarkBGScreen(){return manager.get(darkBgLocation);}

    public void loadShopBG(){
        manager.load(shopBg, Texture.class);
    }
    public Texture getShopBG(){return manager.get(shopBg);}

    public void loadTitleImage(){
        manager.load(titleImageLocation, Texture.class);
    }
    public Texture getTitleImage(){
        return  manager.get(titleImageLocation);
    }


    public void loadBgImage(){
        manager.load(bgImageLocation, Texture.class);
    }
    public Texture getBgImage(){
        return  manager.get(bgImageLocation);
    }



    public BitmapFont getFont1(){
        return manager.get(font1Location);
    }
    public BitmapFont getFont2(){
        return manager.get(font2Location);
    }

    public BitmapFont getFont3(){
        return manager.get(font3Location);
    }
    public BitmapFont getFont4(){
        return manager.get(font4Location);
    }

    public BitmapFont getFont5(){
        return manager.get(font5Location);
    }


    public void loadFonts(){
        manager.load(font1Location, BitmapFont.class);
        manager.load(font2Location, BitmapFont.class);
        manager.load(font3Location, BitmapFont.class);
        manager.load(font4Location, BitmapFont.class);
        manager.load(font5Location, BitmapFont.class);

    }
}
