package com.davegame.labirint.base;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Created by Dave on 04/11/2019.
 */

public class LabirintEmtySpot {
    private Vector2 location;

    private ArrayList<Vector2> spotConectors;
    private boolean isConected;

    public LabirintEmtySpot(Vector2 location){
        this.location = location;
        spotConectors = new ArrayList<Vector2>();
        isConected = false;
    }

    public void AddConection(int x, int y){
        spotConectors.add(new Vector2(x,y));

    }

    public ArrayList<Vector2> getConnections(){

        return spotConectors;
    }
    public Vector2 getLocation(){

        return location;
    }

}
