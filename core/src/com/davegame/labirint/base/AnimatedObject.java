package com.davegame.labirint.base;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Dave on 20/01/2020.
 */

public class AnimatedObject {

    public Animation<TextureRegion> animation;
    private TextureRegion[] animFrames;

    public AnimatedObject(TextureRegion[] animFrames){
        this.animFrames = animFrames;
    }

    public void createAnimation(float frameDuration, Animation.PlayMode playMode){
        animation = new Animation<TextureRegion>(frameDuration, animFrames);
        animation.setPlayMode(playMode);

    }
}
