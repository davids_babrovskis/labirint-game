package com.davegame.labirint.base;


import com.badlogic.gdx.Game;
import com.davegame.labirint.LabirintMain;
import com.davegame.labirint.MainMenu;
import com.davegame.labirint.objects.DaveGame;

/**
 * Created by Dave on 09/09/2019.
 */

public class LevelLoader extends DaveGame {

   /* public LabirintMain loadMainLevel (){
        LabirintMain labMain = new LabirintMain(this);
        return labMain;
    }*/

    @Override
    public void create() {
        this.setScreen(loadMenu());
    }

    public MainMenu  loadMenu(){
        MainMenu menu = new MainMenu(this);
        return  menu;

    }
}
