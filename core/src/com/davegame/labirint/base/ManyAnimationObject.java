package com.davegame.labirint.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

/**
 * Created by Dave on 20/01/2020.
 */

public class ManyAnimationObject extends GameObject {
    private float elapsedTime;
    private Animation<TextureRegion> activeAnim;
    private String activeName;
    private HashMap<String, Animation<TextureRegion>> animationStorage;


    public ManyAnimationObject(){
        super();
        elapsedTime =0;
        activeAnim = null;
        activeName = "emtyAnim";

        animationStorage = new HashMap<String, Animation<TextureRegion>>();
    }

    public void storeAnimation(String name, Animation<TextureRegion> anim)
    {
        animationStorage.put(name,anim);
        if(activeName == null){
            setActiveAnimation(name);
        }
    }

    public void storeAnimation(String name, Texture text){
        TextureRegion reg = new TextureRegion(text);
        TextureRegion[] frames = {reg};

        Animation<TextureRegion> anim = new Animation<TextureRegion>(1.0f, frames);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        storeAnimation(name,anim);

    }

    public void setActiveAnimation(String name){
        if(!animationStorage.containsKey(name)){

            return;
        }

        //No need set animation if current animation is already running in background!

        //if(activeName.equals(name) ){return;}
        if(activeName.equals(name)){return;}

        activeName= name;
        activeAnim =  animationStorage.get(name);
        elapsedTime = 0;


        TextureRegion tex = activeAnim.getKeyFrame(0);
        setWidth(tex.getRegionWidth());
        setHeight(tex.getRegionHeight());

    }

    public String getAnimationName(){
        return activeName;
    }

    @Override
    public void act(float dt){
        super.act(dt);
        elapsedTime += dt;
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
//        if(activeAnim != null){
//            region.setRegion(activeAnim.getKeyFrame(elapsedTime));
//        }
        region.setRegion(activeAnim.getKeyFrame(elapsedTime));
        super.draw(batch, parentAlpha);

    }
}
