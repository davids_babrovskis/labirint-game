package com.davegame.labirint.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.compression.lzma.Base;
import com.davegame.labirint.objects.Coin;
import com.davegame.labirint.objects.Ghost;
import com.davegame.labirint.objects.LabirintCompleteText;
import com.davegame.labirint.objects.LevelExit;
import com.davegame.labirint.objects.LevelTitle;
import com.davegame.labirint.objects.Shield;
import com.davegame.labirint.objects.TorchBonuss;
import com.davegame.labirint.objects.Wall;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Dave on 04/11/2019.
 */

public class LevelGenerator2 {
    public  int[][] map;

    private Random rand;


    public  ArrayList<Wall> walList;
    public  ArrayList<Ghost> ghostList;
    public static  ArrayList<Coin> coinList;
    public static  ArrayList<TorchBonuss> torchBonusList;

    public static ArrayList<Shield> shieldList;

    public static Coin coinToDelete;
    public static TorchBonuss torchToDelete;
    public static Shield shieldToDelete;



    public static int dimensions;

    private LevelExit lvlExit;

    public static Vector2 newHeroPosition;

    private Vector2 chosenExit;


    private boolean exitCreatedInMap;

    private static int generationLuck;

    private static int ghostChance;


    private TextureAtlas atlas;

    private Stage mainStage;


    static {
        generationLuck =1;

        /*map = new int[][]{
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        };*/
    }

    public LevelExit getLevelExit(){

        return  lvlExit;
    }

    public static void setGenerationLuck(int value){
        generationLuck = 1+ value;
      //  BaseScreen.writeLog("Luck is " + generationLuck);
    }

    public void printMaze(){
        for(int i=0; i<map.length; i++){
            String text="";
            for(int l=0; l< map[0].length; l++){
                text+= "" + map[i][l];
            }

           // BaseScreen.writeLog(text);
        }

    }

    public static void resetGhostChance(){
        ghostChance=1;
    }


    public static void increaseLuck(){
        generationLuck++;
        //BaseScreen.writeLog("Luck is " + generationLuck);
    }
    public LevelGenerator2(TextureAtlas atlas, Stage mainStage){

        this.mainStage = mainStage;
        walList = new ArrayList<Wall>();
        ghostList = new ArrayList<Ghost>();
        coinList = new ArrayList<Coin>();

        torchBonusList = new ArrayList<TorchBonuss>();


        shieldList = new ArrayList<Shield>();




        this.atlas = atlas;

        ghostChance=1;
        exitCreatedInMap =false;

        rand = new Random();
        dimensions =4;

        lvlExit = new LevelExit(0,0, atlas);
        mainStage.addActor(lvlExit);

    }



    private int countWallsToCreate(){
        int wallCount=0;

        for(int i=0; i<map.length; i++){
            for(int l=0; l<map[0].length; l++) {
                if(map[i][l] == 0){
                    wallCount++;

                }

            }
        }

        return wallCount;

    }
    private void clearGhostList(){

        ArrayList<Ghost> ghostsToDelete = new ArrayList<Ghost>();

        for(int i=0; i<ghostList.size(); i++){
            ghostsToDelete.add(ghostList.get(i));
            ghostList.get(i).dispose();
            ghostList.get(i).disposeActor();

        }
        for(Ghost ghost :ghostsToDelete){


            ghost.disposeActor();
            ghostList.remove(ghost);
        }



    }



    private void clearShieldList(){
        ArrayList<Shield> shieldToDelete = new ArrayList<Shield>();

        for(int i=0; i<shieldList.size(); i++){
            shieldToDelete.add(shieldList.get(i));
        }
        for(Shield shield :shieldToDelete){

            shield.remove();
            shield.disposeActor();
            //ghostList.remove(ghost);
            shieldList.remove(shield);
        }

    }


    private void clearCoinList(){

        ArrayList<Coin> coinToDelete = new ArrayList<Coin>();

        for(int i=0; i<coinList.size(); i++){
            coinToDelete.add(coinList.get(i));


        }
        for(Coin coin :coinToDelete){


            coin.disposeActor();
            coinList.remove(coin);

        }



    }



    private void clearTorchBList(){
        ArrayList<TorchBonuss> torchBonussesToDelete = new ArrayList<TorchBonuss>();

        for(int i=0; i<torchBonusList.size(); i++){
            torchBonussesToDelete.add(torchBonusList.get(i));
            torchBonusList.get(i).dispose();
            torchBonusList.get(i).disposeActor();

        }
        for(TorchBonuss torchBon :torchBonussesToDelete){

            torchBon.remove();
            torchBonusList.remove(torchBon);
        }

    }

    public static void deleteCoinFromList(){


        //coin.dispose();
        //coin.remove();

        if(coinToDelete !=null){
            coinToDelete.disposeActor();
            coinList.remove(coinToDelete);

        }


    }


    public static void deleteTorchBonuss(){


        if(torchToDelete !=null){
            torchToDelete.disposeActor();
            torchBonusList.remove(torchToDelete);

        }

    }

    public static void deleteShieldFromList(){
        if(shieldToDelete != null){
            shieldToDelete.disposeActor();
            shieldList.remove(shieldToDelete);

        }



    }

    public void createLevel(){
       // if(LevelTitle.labirintNumber % 2 ==0){
            ghostChance++;
       // }

        createMazeBounds();
        addLevelExit();



        addObjectToLabirint();
       // printMaze();

        if(ghostList.size() >0){
            clearGhostList();

        }
        if(coinList.size() >0){
            clearCoinList();
        }

        if(torchBonusList.size() >0){
            clearTorchBList();
        }

        if(shieldList.size() >0){
            clearShieldList();
        }



        int wallCount = countWallsToCreate();

       // BaseScreen.writeLog("Total wall count is " + wallCount + " wall list size is " + walList.size());

        //Creating wall
        if(wallCount >walList.size()){
            //create new walls
           int  wallsToCreate = wallCount -walList.size();
            for(int i=0; i<wallsToCreate; i++){
                createWall(0,0);

            }

        }else if(wallCount <walList.size()){
            //delete walls
            int wallToDelete = walList.size() - wallCount;
            ArrayList<Wall> wallsToDeleteList = new ArrayList<Wall>();

            for(int i=0; i<wallToDelete; i++){
                wallsToDeleteList.add(walList.get(i));
                walList.get(i).dispose();
                walList.get(i).remove();

            }
            for(Wall wall : wallsToDeleteList){

                wall.remove();
                walList.remove(wall);
            }




        }

        int randWall = rand.nextInt(25);

        Wall.createColor();

        for(int i=0; i<walList.size(); i++){
            walList.get(i).setWallTexture(randWall);

        }


     //   BaseScreen.writeLog("Total wall count is " + wallCount + " wall list size is " + walList.size());

        for(int i=0; i<map.length; i++){
            for(int l=0; l< map[0].length; l++){
                if(map[i][l] ==0){
                    //createWall(200 * l,200 *i);
                    walList.get(wallCount -1).setPosition(200*l,200*i);
                    wallCount--;

                }
                if(map[i][l]==3){
                    lvlExit.setLevelExitPosition(200 * l,200 *i);

                }
                /*if(map[i][l]==4){
                    createGhost(200 * l,200 *i);

                }

                if(map[i][l]==5){
                    createCoins((200 * l) +80,(200 *i)+80);

                }

                if(map[i][l]==6){
                    createTorchBonus((200 * l) +80,(200 *i)+80);

                }*/

            }
        }

        for(int i=0; i<map.length; i++){
            for(int l=0; l< map[0].length; l++){
                if(map[i][l]==4){
                    createGhost(200 * l,200 *i);

                }

                if(map[i][l]==5){
                    createCoins((200 * l) +80,(200 *i)+80);

                }

                if(map[i][l]==6){
                    createTorchBonus((200 * l) +80,(200 *i)+80);

                }

                if(map[i][l]==7){
                    createShieldBonus((200 * l) +80,(200 *i)+80);

                }




            }

        }



    }

//Good function for adding ghosts, coins,......
    public void addObjectToLabirint(){
        boolean ghostAdded = false;

        for(int i=0; i<map.length; i++){
            for(int l=0; l< map[0].length; l++){
                if(map[i][l]==1 && i!= newHeroPosition.x && l!= newHeroPosition.y && i!= chosenExit.x && l!= chosenExit.y ){
                    //position is good for ghost
                    int objectChance =rand.nextInt(100-1)+1;

                    //Create enemy in map
                    if(objectChance >(96 - ghostChance)){
                        map[i][l] = 4;
                        ghostAdded = true;
                        //BaseScreen.writeLog("Must create ghost!");

                    }
                    //Create coin in map
                    if(objectChance <40+generationLuck){
                        map[i][l] = 5;

                    }
                    //Create torch Bonuss in maze
                    if(objectChance >= 74 && objectChance <= 75+generationLuck){
                        map[i][l] = 6;

                    }

                    //Create shield
                    if(objectChance > 75 && objectChance < 80 && ghostAdded ){
                        map[i][l] = 7;
                       // BaseScreen.writeLog("Must create shield!");

                    }

                }


            }
        }

    }



    private void createCoins(float positionX, float positionY){
        Coin newCoin = new Coin(positionX,positionY, atlas);
        mainStage.addActor(newCoin);
        coinList.add(newCoin);

    }

    private void createGhost(float positionX, float positionY){
        Ghost newGhost = new Ghost(positionX, positionY, atlas, walList);
        mainStage.addActor(newGhost);
        ghostList.add(newGhost);
    }

    private void createTorchBonus(float positionX, float positionY){
        TorchBonuss torchBonus = new TorchBonuss(positionX, positionY, atlas);
        mainStage.addActor(torchBonus);
        torchBonusList.add(torchBonus);

    }

    private void createShieldBonus(float positionX, float positionY){
        Shield shield = new Shield(positionX,positionY, atlas);
        mainStage.addActor(shield);
        shieldList.add(shield);

    }

    public void removeOldExitInMap(){
        //Removes old exit
        if(exitCreatedInMap == true){
            for(int i=0; i<map.length ; i++){
                for(int k=0;k<map[0].length;k++){
                    if(map[i][k] ==3){
                        map[i][k]=1;
                        exitCreatedInMap = false;
                        return;

                    }


                }}


        }


    }


    public int chechkHowGoodPositionIsForExit(int x, int y){
        int wallCountAround =0;
        if(map[x-1][y]==0)wallCountAround++;
        if(map[x+1][y]==0)wallCountAround++;
        if(map[x][y-1]==0)wallCountAround++;
        if(map[x][y+1]==0)wallCountAround++;

        return wallCountAround;

    }



    public void addLevelExit(){


        removeOldExitInMap();

        ArrayList<Vector2> positionsToAddExit = new ArrayList<Vector2>();

        for(int i=0; i<map.length ; i++){
            for(int k=0;k<map[0].length;k++){

                if(map[i][k] ==1){
                    if(chechkHowGoodPositionIsForExit(i,k) ==3){
                        positionsToAddExit.add(new Vector2(i,k));
                    }

                }



            }

        }


        //Vector2 chosenExit = positionsToAddExit.get(rand.nextInt(positionsToAddExit.size()));
        newHeroPosition =positionsToAddExit.get(rand.nextInt(positionsToAddExit.size()));

        positionsToAddExit.remove(newHeroPosition);
        chosenExit =longestDistanceVector(positionsToAddExit,newHeroPosition);

        map[(int)chosenExit.x][(int)chosenExit.y] = 3;


        exitCreatedInMap=true;

        //newHeroPosition =longestDistanceVector(positionsToAddExit,chosenExit);
        // positionsToAddExit.get(rand.nextInt(positionsToAddExit.size()));


    }


    private Vector2 longestDistanceVector(ArrayList<Vector2> positionArray,Vector2 exitPosition){
        Vector2 longestVector=new Vector2(0,0);
        double longestLine=0;

        for(Vector2 vect2:positionArray){
            double newLineLenght = Math.sqrt(Math.pow(exitPosition.x - vect2.x,2) +Math.pow(exitPosition.y - vect2.y,2));

            if(newLineLenght> longestLine){
                longestLine =newLineLenght;
                longestVector.set(vect2.x,vect2.y);

            }
        }



        return  longestVector;
    }

    private void createMazeBounds(){
        map = new int[dimensions+2][dimensions+2];
        Generator generator = new Generator(dimensions);
        generator.generateMaze();
        int[][] mapForTime =generator.returnMaze();

       // map =generator.returnMaze();
        for(int i=0; i< dimensions+2; i++){
            for(int k=0; k<dimensions+2; k++){
                if(i ==0 || i== dimensions +1){
                    map[i][k] =0;
                }else if(k==0 || k == dimensions +1){
                    map[i][k] =0;
                }else{
                    map[i][k] = mapForTime[i-1][k-1];
                }


            }
        }

    }
    private void createWall(float x, float y){
        Wall wall = new Wall(atlas);
        wall.setPosition(x,y);
        walList.add(wall);
        mainStage.addActor(wall);

    }




    public void clearStage(){
        clearCoinList();
        clearTorchBList();
        clearGhostList();
        removeOldExitInMap();
        lvlExit.remove();
        deleteAllWallss();




      /*  lvlExit.remove();
        walList = null;
        torchBonusList = null;
        ghostList = null;
        coinList = null;*/


    }

    private void deleteAllWallss(){

        ArrayList<Wall> wallsToDeleteList = new ArrayList<Wall>();

        for(int i=0; i<walList.size(); i++){
            wallsToDeleteList.add(walList.get(i));
        }
        for(Wall wall : wallsToDeleteList){

            wall.disposeActor();
            walList.remove(wall);
        }

    }







}
