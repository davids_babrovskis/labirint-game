package com.davegame.labirint;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.davegame.labirint.base.BaseScreen;

import com.davegame.labirint.base.LevelGenerator2;

import com.davegame.labirint.objects.AllShopButtons;
import com.davegame.labirint.objects.CoinCountText;
import com.davegame.labirint.objects.DarkBackground;
import com.davegame.labirint.objects.DaveGame;
import com.davegame.labirint.objects.GameButton;
import com.davegame.labirint.objects.GameData;
import com.davegame.labirint.objects.GameOverAnim;
import com.davegame.labirint.objects.GamePad;
import com.davegame.labirint.objects.GameTask;
import com.davegame.labirint.objects.Hero;
import com.davegame.labirint.objects.LabirintCompleteText;
import com.davegame.labirint.objects.LevelTitle;

import com.davegame.labirint.objects.PauseScreen;
import com.davegame.labirint.objects.PlayerData;
import com.davegame.labirint.objects.ShieldTime;
import com.davegame.labirint.objects.ShopScreen;
import com.davegame.labirint.objects.SignalHandler;
import com.davegame.labirint.objects.Torch;

public class LabirintMain extends BaseScreen {
	public static Hero hero;
	private Camera cam;

	private GamePad gamePad;

	private Vector3 gamePadNewPos;
	private Vector3 gamePadDraggedPos;
	private LevelGenerator2 lg2;
	//public Wall wall;

	private float elapsed;

	private GameButton resetButton;
	private GameButton pauseButton;

	private LabirintCompleteText labirintComleteTxt;

	private PauseScreen pauseScreen;

	private GameButton shopButton;
	private GameButton exitButton;
	private GameButton resumeButton;

	LevelTitle lvlTitle;


	private CoinCountText coinCount;

	private DarkBackground darkBackground;

	private ShopScreen shopScreen;


	private GameButton exitShopButton;

	private Torch lightTorch;


	private AllShopButtons allShButt;


	GameOverAnim gameOverAnim;

	private ShieldTime shieldTime;

	private PlayerData plyData;



	private TextureAtlas atlas;

	private Music[] gameLoops;
	private Music curentMusic;

	private Sound levelCompleteSound;
	private Sound buttonPressSound;



	private GameTask gmTask;

	private float showTaskTime=0;


	private Sound scarySound;

	public LabirintMain(DaveGame g){
		super(g);
	}

	GameButton soundButton;
	@Override
	public void update(float delta) {
			//super.setCameraPosition(hero.getX(), hero.getY());
		if(gmTask.isVisible()){
			showTaskTime +=delta;

			if(showTaskTime >=3){
				playMusic(curentMusic);
				curentMusic.setLooping(true);
				showTaskTime =0;
				pauseButton.setVisible(true);

				gmTask.setVisible(false);

				lvlTitle.setVisible(true);
				coinCount.setVisible(true);


			}
		}


		atlas = super.game.gameAssets.getTextureAtlas();
		cam.position.set(hero.getX()+ hero.getOriginX(), hero.getY() + hero.getOriginY(),0);
		//cam.position.x = MathUtils.clamp(cam.position.x,viewWidth/2, MAP_WIDTH - viewWidth/2);
		//cam.position.y = MathUtils.clamp(cam.position.y,viewHeight/2, MAP_HEIGHT - viewHeight/2);
		cam.update();
		if(Hero.returnLightB() == true && Hero.mustLoadNewLevel == false){
			BaseScreen.paused = true;
			labirintComleteTxt.showGameOverText();
			curentMusic.pause();
			gameOverAnim.togggleVisible();

			shopButton.setVisible(true);
			pauseButton.setVisible(false);
			resetButton.setVisible(true);
			exitButton.setVisible(true);
		}


		if(Hero.mustLoadNewLevel && Hero.returnLightB() ==false){
			if(curentMusic.isPlaying()){
				curentMusic.pause();

				playSound(levelCompleteSound);
			}
			if(DarkBackground.canChange == true){
				DarkBackground.canChange = false;
				pauseButton.setVisible(false);
			}

			labirintComleteTxt.showEndLevelLabel();

			elapsed+=delta;
			if(elapsed>2.0){
				DarkBackground.canChange = true;
				pauseButton.setVisible(true);
				prepareNewLevel();
				playMusic(curentMusic);



				elapsed=0;
				labirintComleteTxt.hideLabel();
			}

		}




	}

	public void prepareNewLevel(){

		gmTask.scaleActionReset();
		lg2.createLevel();
		hero.setWallAndGhost(lg2.walList, lg2.ghostList);
		Hero.mustLoadNewLevel = false;
		//lg2.deleteLevel();
		LevelGenerator2.dimensions++;
		hero.resetHeroPosition();
		LevelTitle.increaseLevelNumber();







	}

	@Override
	public void render(float delta) {
		super.render(delta);
		//super.returnCamera().position.set(new Vector3(hero.getX(), hero.getY(),0));

	}

	@Override
	public void create () {

		gameLoops = new Music[3];

		gameLoops[0] = super.game.gameAssets.getLoop1();
		gameLoops[1] = super.game.gameAssets.getLoop2();
		gameLoops[2] = super.game.gameAssets.getLoop3();


		levelCompleteSound = super.game.gameAssets.getLevelCpSound();
		buttonPressSound = super.game.gameAssets.getButtonClick();


		curentMusic = gameLoops[MathUtils.random(MathUtils.random(2))];
		scarySound = super.game.gameAssets.getScarySound();



		atlas = super.game.gameAssets.getTextureAtlas();


		lg2 = new LevelGenerator2(atlas, super.mainStage);
		lg2.createLevel();


		playSound(scarySound);

		hero = new Hero(lg2.getLevelExit(), super.game.gameAssets);
		hero.setWallAndGhost(lg2.walList, lg2.ghostList);

		hero.resetHeroPosition();



		gamePad = new GamePad(atlas);

		gamePadNewPos = new Vector3();
		gamePadDraggedPos = new Vector3();


		lvlTitle = new LevelTitle(game.gameAssets);


		labirintComleteTxt = new LabirintCompleteText(game.gameAssets);


		coinCount =new  CoinCountText(this.game.gameAssets);


		lightTorch = new Torch(hero, atlas);


		soundButton = new GameButton(new TextureRegionDrawable(new TextureRegion(super.game.gameAssets.getSoundOnImg())), null, new TextureRegionDrawable(new TextureRegion(super.game.gameAssets.getSoundOfImg())),super.viewWidth *0.85f, super.viewHeight * 0.8f);
		soundButton.setVisible(false);
		if(BaseScreen.sounOn== false){
			soundButton.toggle();
		}



		resumeButton = new GameButton(new TextureRegionDrawable(atlas.findRegion("resumeButton")), BaseScreen.viewWidth *0.3f, BaseScreen.viewHeight *0.67f);
		resumeButton.setVisible(false);
		addListenerToResumuButton();

		resetButton = new GameButton(new TextureRegionDrawable(atlas.findRegion("restartButton")), BaseScreen.viewWidth *0.3f, BaseScreen.viewHeight *0.67f);
		resetButton.setVisible(false);
		addListenerToRetryButton();





		pauseButton = new GameButton(new TextureRegionDrawable(atlas.findRegion("PauseButton")), null, new TextureRegionDrawable(atlas.findRegion("PauseButton1")),super.viewWidth *0.85f, super.viewHeight * 0.9f);
		pauseButton.setVisible(false);
		addListenerToPauseButton();
		pauseScreen = new PauseScreen(super.game.gameAssets);


		shopButton = new GameButton(new TextureRegionDrawable(atlas.findRegion("shopButton")), BaseScreen.viewWidth *0.3f, BaseScreen.viewHeight *0.6f);
		shopButton.setVisible(false);
		addListenerToShopButton();


		exitButton = new GameButton(new TextureRegionDrawable(atlas.findRegion("ExitButton")), BaseScreen.viewWidth *0.3f, BaseScreen.viewHeight *0.53f);
		exitButton.setVisible(false);


		addListenerToExitButton();

		shopScreen = new ShopScreen(super.game.gameAssets);
		gameOverAnim = new GameOverAnim(BaseScreen.viewWidth *0.4f, BaseScreen.viewHeight*0.3f, super.game.gameAssets);
		exitShopButton = new GameButton(new TextureRegionDrawable(atlas.findRegion("leaveShop")), BaseScreen.viewWidth *0.3f, BaseScreen.viewHeight *0.1f);
		exitShopButton.setVisible(false);



		coinCount.setVisible(false);
		lvlTitle.setVisible(false);

		gameOverAnim.togggleVisible();



		addListenerToExitShopButton();


		shieldTime = new ShieldTime(atlas);

		/*
		MoveButton moveButtonUp= new MoveButton(150f,250f);
		MoveButton moveButtonLeft = new MoveButton(250f,150f);
		MoveButton moveButtonRight = new MoveButton(50f,150f);
		MoveButton moveButtonDown = new MoveButton(150f,60f);
		moveButtonLeft.rotateBy(-90f);
		moveButtonRight.rotateBy(90f);
		moveButtonDown.rotateBy(-180);

		moveButtonUp.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Hero.MoveUp();
				return super.touchDown(event, x, y, pointer, button);

			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				Hero.StopMovingY();
			}
		});



		moveButtonRight.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Hero.MoveLeft();
				return super.touchDown(event, x, y, pointer, button);

			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				Hero.StopMovingX();
			}
		});

		moveButtonLeft.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Hero.MoveRight();
				return super.touchDown(event, x, y, pointer, button);

			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				Hero.StopMovingX();
			}
		});

		moveButtonDown.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Hero.MoveDown();
				return super.touchDown(event, x, y, pointer, button);

			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				Hero.StopMovingY();
			}
		});



		*/



		darkBackground = new DarkBackground(game.gameAssets);
		gmTask = new GameTask(super.game.gameAssets);


		super.mainStage.addActor(hero);
		super.uiStage.addActor(darkBackground);
		uiStage.addActor(gamePad);
		uiStage.addActor(lvlTitle);
		uiStage.addActor(labirintComleteTxt.labelContainer);

		uiStage.addActor(resumeButton);
		uiStage.addActor(resetButton);
		uiStage.addActor(pauseButton);

		uiStage.addActor(shopButton);
		uiStage.addActor(exitButton);
		uiStage.addActor(gameOverAnim);

		uiStage.addActor(shieldTime);

		uiStage.addActor(shopScreen);
		uiStage.addActor(pauseScreen);
		uiStage.addActor(coinCount);
		uiStage.addActor(shopButton);
		uiStage.addActor(exitShopButton);

		uiStage.addActor(soundButton);


		/*





		super.uiStage.addActor(moveButtonUp);
		super.uiStage.addActor(moveButtonLeft);
		super.uiStage.addActor(moveButtonRight);
		super.uiStage.addActor(moveButtonDown);
		*/


		allShButt = new AllShopButtons(super.game.gameAssets, uiStage);

		cam = mainStage.getCamera();
		mainStage.addActor(lightTorch);


		plyData = new PlayerData();

		addListenerToSoundBt();

		uiStage.addActor(gmTask);
		//super.mainStage.addActor(wall);
	}


	private void togglePauseButtonVisibility(){
		shopButton.setVisible(!shopButton.isVisible());
		exitButton.setVisible(!exitButton.isVisible());
		soundButton.setVisible(!soundButton.isVisible());
	}
	private void addListenerToExitButton(){
		exitButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//Gdx.app.log("BrickBreaker 2", "Button was pressed!");
				//pauseButton.background(resumeButtonTexture);
				playSound(buttonPressSound);


				SignalHandler.getInstance().dispatchSetCoins(GameData.getInstance().getCoinCount());


				hero.resetHeroColor();
				LevelGenerator2.resetGhostChance();
				lightTorch.restoreTorchSize();
				dispose();



				game.setScreen(new MainMenu(game));
				BaseScreen.paused = false;
				DarkBackground.resetSpriteNumber();
				CoinCountText.saveCoinCount();


			}


		});

	}

	private void addListenerToRetryButton(){

				resetButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//Gdx.app.log("BrickBreaker 2", "Button was pressed!");
				//pauseButton.background(resumeButtonTexture);
				curentMusic = gameLoops[MathUtils.random(MathUtils.random(2))];
				if(!curentMusic.isPlaying()){
					curentMusic.setPosition(0);
					playMusic(curentMusic);
					curentMusic.setLooping(true);
				}

				if(gameOverAnim.isVisible()){
					gameOverAnim.togggleVisible();
				}

				playSound(buttonPressSound);

				pauseButton.setVisible(true);
				BaseScreen.paused = false;
				labirintComleteTxt.hideLabel();
				DarkBackground.resetDarkBackground();
				lightTorch.restoreTorchSize();
				resetButton.setVisible(false);
				Hero.setNoLightToFalse();



				LevelGenerator2.dimensions=4;
				lg2.createLevel();
				LevelTitle.labirintNumber = 1;

				//lg2.deleteLevel();

				hero.resetHeroPosition();
				hero.resetHeroColor();
				shopButton.setVisible(false);
				exitButton.setVisible(false);



			}


		});


	}


	private void addListenerToPauseButton(){
		pauseButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//Gdx.app.log("BrickBreaker 2", "Button was pressed!");
				//pauseButton.background(resumeButtonTexture);
				setGameActive();

				playSound(buttonPressSound);


			}


		});

	}
	private void setGameActive(){


		if(curentMusic.isPlaying()){

			curentMusic.pause();

		}else{
			//if(sounOn){
			playMusic(curentMusic);
			//}
		}

		resumeButton.setVisible(!resumeButton.isVisible());
		BaseScreen.togglePause();
		DarkBackground.toggleChange();
		pauseScreen.togglePauseScreen();
		togglePauseButtonVisibility();
	}

	@Override
	public void hide() {
		super.hide();



	}

	@Override
	public void pause() {
		super.pause();
		//if(BaseScreen.paused == false && Hero.mustLoadNewLevel == false && gmTask.isVisible() == false){
			//curentMusic.pause();
			//setGameActive();
		BaseScreen.paused =true;
		curentMusic.pause();

		//}

	}

	@Override
	public void resume() {
		super.resume();
		BaseScreen.paused =false;
		curentMusic.play();
	}

	public void addListenerToResumuButton(){
		resumeButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//Gdx.app.log("BrickBreaker 2", "Button was pressed!");
				//pauseButton.background(resumeButtonTexture);

				playSound(buttonPressSound);

				GameOverAnim.mustShowBestScore = false;


				setGameActive();




			}


		});

	}

	public void addListenerToShopButton(){
		shopButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//Gdx.app.log("BrickBreaker 2", "Button was pressed!");
				//pauseButton.background(resumeButtonTexture);
				if(pauseScreen.isVisible() == false){
					pauseScreen.setVisible(true);
				}
				playSound(buttonPressSound);
				allShButt.toggleShopButtonVisibility();
				shopScreen.toggleVisible();
				pauseScreen.setShopTexture();
				exitShopButton.setVisible(true);

			}


		});

	}

	public void addListenerToSoundBt(){
		soundButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {

				switchSound();

				//playMusic(curentMusic);

			}


		});
	}

	public void addListenerToExitShopButton(){
		exitShopButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {

				playSound(buttonPressSound);
				//Gdx.app.log("BrickBreaker 2", "Button was pressed!");
				//pauseButton.background(resumeButtonTexture);
				//DarkBackground.resetDarkBackground();
				shopScreen.toggleVisible();
				pauseScreen.setPauseTxtTexture();
				exitShopButton.setVisible(false);
				allShButt.toggleShopButtonVisibility();

				if(Hero.returnLightB() == true){
					pauseScreen.setVisible(false);
				}

			}


		});


	}


	@Override
	public void dispose () {
		super.dispose();

		/*for(int i=0; i<gameLoops.length; i++){
			gameLoops[i].dispose();
		}*/

		gmTask.scaleActionReset();

		gmTask.disposeActor();
		soundButton.disposeButton();
		exitButton.disposeButton();
		shopButton.disposeButton();
		pauseButton.disposeButton();
		pauseScreen.disposeActor();
		resumeButton.disposeButton();
		shopScreen.disposeActor();
		exitShopButton.disposeButton();
		allShButt.removeButtons();

		lightTorch.disposeActor();

		lvlTitle.disposeActor();
		coinCount.disposeActor();

		hero.disposeActor();

		lg2.clearStage();


		darkBackground.disposeActor();

		gameOverAnim.disposeActor();

		LevelTitle.resetLevelNumber();

		labirintComleteTxt.dispose();



		if(DarkBackground.canChange == false){
			DarkBackground.canChange = true;
			DarkBackground.resetSpriteNumber();


		}




		Hero.setNoLightToFalse();
		gameOverAnim.remove();




		lg2.clearStage();


		//uiStage.dispose();
		//mainStage.dispose();
		mainStage.clear();
		uiStage.clear();



		//mainStage.dispose();
		//uiStage.dispose();


	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Input.Keys.A){ Hero.MoveLeft();}
		else if(keycode == Input.Keys.D){ Hero.MoveRight();}
		else if(keycode == Input.Keys.W){ Hero.MoveUp();}
		else if(keycode == Input.Keys.S){Hero.MoveDown();}
		else if(keycode == Input.Keys.C){
			//GameData.getInstance().increaseCoinCount(100);
			SignalHandler.getInstance().dispatchIncreaseCoins(100);
		}else if(keycode == Input.Keys.R){
			//GameData.getInstance().increaseCoinCount(100);
			PlayerData.clearAllPref();
			BaseScreen.writeLog("Game Data reseted");
		}

		return super.keyDown(keycode);
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.A){ Hero.StopMovingX();}
		else if(keycode == Input.Keys.D){ Hero.StopMovingX();}
		else if(keycode == Input.Keys.W){ Hero.StopMovingY();}
		else if(keycode == Input.Keys.S){ Hero.StopMovingY();}
		return super.keyUp(keycode);
	}

	@Override
	public boolean keyTyped(char character) {
		return super.keyTyped(character);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		/*if(screenX < (BaseScreen.viewWidth/2) && screenY < BaseScreen.viewHeight *0.7 && screenY > BaseScreen.viewHeight *0.3 ){
			Hero.MoveLeft();
		}else if(screenX > (BaseScreen.viewWidth/2) && screenY < BaseScreen.viewHeight *0.7 && screenY > BaseScreen.viewHeight *0.3 ){
			Hero.MoveRight();
		}else if(screenY > BaseScreen.viewHeight *0.7){
			Hero.MoveDown();
		}else if(screenY < BaseScreen.viewHeight *0.3){
			Hero.MoveUp();
		}*/

		if(Hero.returnLightB() == false){

			gamePadNewPos.set(Gdx.input.getX(),Gdx.input.getY(),0);
			super.unprojectWithCamera(gamePadNewPos);
			//Upper corner of screen is blocked
			if(gamePadNewPos.y < BaseScreen.viewHeight -250 && BaseScreen.paused == false){
				gamePad.setGamePadVisible(gamePadNewPos.x,gamePadNewPos.y);
			}



		}


		return super.touchDown(screenX, screenY, pointer, button);

	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		gamePad.hideGamePad();


		return super.touchUp(screenX, screenY, pointer, button);
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		if(Hero.returnLightB() == false){
			gamePadDraggedPos.set(screenX,screenY,0);
			super.unprojectWithCamera(gamePadDraggedPos);

			gamePad.setPadMovePOsition(gamePadDraggedPos.x, gamePadDraggedPos.y);


		}


		return super.touchDragged(screenX, screenY, pointer);
	}

	private void playSound(Sound sound){
		if(BaseScreen.sounOn){
			sound.play();
		}
	}

	private void playMusic(Music music){
		if(BaseScreen.sounOn){
			music.play();
		}
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return super.mouseMoved(screenX, screenY);
	}

	@Override
	public boolean scrolled(int amount) {
		return super.scrolled(amount);
	}
}
