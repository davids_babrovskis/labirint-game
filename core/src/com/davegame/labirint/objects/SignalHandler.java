package com.davegame.labirint.objects;

import com.davegame.labirint.interfaces.InterfaceChangeBestScore;
import com.davegame.labirint.interfaces.InterfaceChangeCoinValue;

import java.util.ArrayList;

/**
 * Created by Dave on 08/02/2020.
 */

public class SignalHandler {

    private static SignalHandler singleInstance=null;
    private ArrayList<InterfaceChangeCoinValue> coinInterfaceArray;
    private ArrayList<InterfaceChangeBestScore> bestScoreArrayList;




    private SignalHandler(){
        this.coinInterfaceArray = new ArrayList<InterfaceChangeCoinValue>();
        this.bestScoreArrayList = new ArrayList<InterfaceChangeBestScore>();

    }


    public static SignalHandler getInstance(){
        if(singleInstance == null){
            singleInstance = new SignalHandler();
        }

        return  singleInstance;

    }

    public void subscribeToChangeCoin(InterfaceChangeCoinValue entity){
        coinInterfaceArray.add(entity);
    }

    public void dispatchIncreaseCoins(int value ){
        for(InterfaceChangeCoinValue entity: this.coinInterfaceArray){
            entity.increaseCoin(value);
        }
    }


    public void dispatchSetCoins(int value ){
        for(InterfaceChangeCoinValue entity: this.coinInterfaceArray){
            entity.setCoin(value);
        }
    }


    public void subscribeToBestScoreChange(InterfaceChangeBestScore entity){
        bestScoreArrayList.add(entity);
    }


    public void dispathBestScoreChanged(int value){
        for(InterfaceChangeBestScore entity: bestScoreArrayList){
            entity.bestScoreChanged(value);

        }
    }

}
