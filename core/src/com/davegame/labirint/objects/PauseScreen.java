package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;
import com.davegame.labirint.interfaces.InterfaceChangeBestScore;

/**
 * Created by Dave on 27/01/2020.
 */

public class PauseScreen extends GameObject implements InterfaceChangeBestScore {

    private TextureRegion pauseTxtTexture;
    private TextureRegion shopTextTexture;

    private BitmapFont font;
    private float scorePositionX, scorePositionY;

    private int bestScore=0;

    private String bestScoreText ="BestScore:" + bestScore;



    public PauseScreen(GameAssets assets){

        SignalHandler.getInstance().subscribeToBestScoreChange(this);
        font = assets.getFont5();
        font.setColor(Color.WHITE);
        font.getData().setScale(2f);




        scorePositionX = BaseScreen.viewWidth *0.28f;
        scorePositionY = BaseScreen.viewHeight *0.78f;


        bestScore = GameData.getInstance().getBestScore();


        bestScoreText ="BestScore:" + bestScore;

        pauseTxtTexture = assets.getTextureAtlas().findRegion("pausedText");
        shopTextTexture = assets.getTextureAtlas().findRegion("shopText");
        super.setOrigin(super.getWidth() /2, super.getHeight() /2);
        super.setVisible(false);
        super.setTextureRegion(pauseTxtTexture);
        super.setPosition(BaseScreen.viewWidth * 0.3f, BaseScreen.viewHeight *0.8f);




    }

    public void setShopTexture(){
        super.setTextureRegion(shopTextTexture);
    }

    public void setPauseTxtTexture(){
        super.setTextureRegion(pauseTxtTexture);

    }

    public void togglePauseScreen(){
        super.setVisible(!super.isVisible());


    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        font.draw(batch, bestScoreText, scorePositionX, scorePositionY);
    }

    @Override
    public void bestScoreChanged(int value) {
        if(bestScore<value){
            GameOverAnim.mustShowBestScore = true;
            bestScore =value;
        }
        bestScoreText = "BestScore:" + bestScore;
    }
}
