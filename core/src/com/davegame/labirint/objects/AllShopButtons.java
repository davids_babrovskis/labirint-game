package com.davegame.labirint.objects;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.compression.lzma.Base;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.LevelGenerator2;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 28/01/2020.
 */

public class AllShopButtons {
    private StatButton moveSpeedBt, flameLgBt, luckBt, shieldBt;

    private CharacterButton catBt, rabbitButton,frogBt, pengiunBt, pigBt, mouseBt,deerBt, beerBt;
    private CharacterButton selectedButton;

    private Sound pressSound;


    public AllShopButtons(GameAssets assetManager, Stage uiStage){
        TextureAtlas atlas = assetManager.getTextureAtlas();

        pressSound = assetManager.getButtonClick();

        moveSpeedBt = new StatButton(atlas.findRegion("moveSpeed"), BaseScreen.viewWidth * 0.1f, BaseScreen.viewHeight *0.6f , 19, atlas);
        moveSpeedBt.setStatLevel(PlayerData.getMoveSpeedLevel());
        if(PlayerData.getMoveSpeedLevel() !=0){
            Hero.resetPadSpeedDecrease();
            Hero.increaseMoveSpeed(PlayerData.getMoveSpeedLevel());
        }

        flameLgBt = new StatButton(atlas.findRegion("lightTime"), BaseScreen.viewWidth * 0.3f, BaseScreen.viewHeight *0.6f,20, atlas);
        flameLgBt.setStatLevel(PlayerData.getLightTime());
        DarkBackground.setTimeToDarkBg(PlayerData.getLightTime());




        luckBt = new StatButton(atlas.findRegion("luck"), BaseScreen.viewWidth * 0.5f, BaseScreen.viewHeight *0.6f,30, atlas);
        luckBt.setStatLevel(PlayerData.getLuck());
        LevelGenerator2.setGenerationLuck(PlayerData.getLuck());



        shieldBt = new StatButton(atlas.findRegion("shieldTimeBt"), BaseScreen.viewWidth * 0.7f, BaseScreen.viewHeight *0.6f,15, atlas);
        shieldBt.setStatLevel(PlayerData.getShieldTime());
        ShieldTime.setTimeWidth(PlayerData.getShieldTime());


        catBt = new CharacterButton(BaseScreen.viewWidth * 0.1f, BaseScreen.viewHeight *0.4f,0,"CAT", true, atlas.findRegion("Cat"),assetManager);

        rabbitButton =  new CharacterButton(BaseScreen.viewWidth * 0.3f, BaseScreen.viewHeight *0.4f,100,"RABBIT", false,atlas.findRegion("rabbit"), assetManager);

        frogBt =  new CharacterButton(BaseScreen.viewWidth * 0.5f, BaseScreen.viewHeight *0.4f,200,"FROG", false,atlas.findRegion("frog"),assetManager);

        pengiunBt = new CharacterButton(BaseScreen.viewWidth * 0.7f, BaseScreen.viewHeight *0.4f,200,"PENGUIN", false,atlas.findRegion("penguin"),assetManager);

        pigBt = new CharacterButton(BaseScreen.viewWidth * 0.1f, BaseScreen.viewHeight *0.25f,200,"PIG", false,atlas.findRegion("Pig"), assetManager);

        mouseBt = new CharacterButton(BaseScreen.viewWidth * 0.3f, BaseScreen.viewHeight *0.25f,200,"MOUSE", false,atlas.findRegion("Mouse"), assetManager);

        deerBt = new CharacterButton(BaseScreen.viewWidth * 0.5f, BaseScreen.viewHeight *0.25f,200,"DEER", false,atlas.findRegion("deer1"), assetManager);

        beerBt = new CharacterButton(BaseScreen.viewWidth * 0.7f, BaseScreen.viewHeight *0.25f,200,"BEER", false,atlas.findRegion("beer1"), assetManager);



        //selectedButton =catBt;

        setSelectedAnimalButton(PlayerData.getPlayerInteger());



        uiStage.addActor(frogBt);
        uiStage.addActor(moveSpeedBt);
        uiStage.addActor(flameLgBt);
        uiStage.addActor(luckBt);
        uiStage.addActor(shieldBt);
        uiStage.addActor(catBt);
        uiStage.addActor(rabbitButton);
        uiStage.addActor(pengiunBt);
        uiStage.addActor(pigBt);
        uiStage.addActor(mouseBt);
        uiStage.addActor(deerBt);
        uiStage.addActor(beerBt);


        toggleShopButtonVisibility();


        addListenersToButtons();




    }


    private void setSelectedAnimalButton(int value){
        switch (value){
            case 0: selectedButton =catBt; catBt.isSelected =true; break;
            case 1: selectedButton =rabbitButton;  rabbitButton.isSelected =true; break;
            case 2: selectedButton =frogBt;  frogBt.isSelected =true; break;
            case 3: selectedButton =pengiunBt;  pengiunBt.isSelected =true; break;
            case 4: selectedButton =pigBt;   pigBt.isSelected =true; break;
            case 5: selectedButton =mouseBt;  mouseBt.isSelected =true; break;
            case 6: selectedButton =deerBt;  deerBt.isSelected =true; break;
            case 7: selectedButton =beerBt;  beerBt.isSelected =true; break;
        }

    }

   /* public void setButtonSelected(int numb){
        switch (numb){
            case 0:

        }


    }*/

    public void toggleShopButtonVisibility(){
        moveSpeedBt.setVisible(!moveSpeedBt.isVisible());
        flameLgBt.setVisible(!flameLgBt.isVisible());
        luckBt.setVisible(!luckBt.isVisible());
        shieldBt.setVisible(!shieldBt.isVisible());

        catBt.setVisible(!catBt.isVisible());
        rabbitButton.setVisible(!rabbitButton.isVisible());
        frogBt.setVisible(!frogBt.isVisible());
        pengiunBt.setVisible(!pengiunBt.isVisible());
        pigBt.setVisible(!pigBt.isVisible());
        mouseBt.setVisible(!mouseBt.isVisible());
        deerBt.setVisible(!deerBt.isVisible());
        beerBt.setVisible(!beerBt.isVisible());

    }

    public void removeButtons(){
        moveSpeedBt.disposeActor();
        flameLgBt.disposeActor();
        luckBt.disposeActor();
        shieldBt.disposeActor();

        catBt.disposeActor();
        rabbitButton.disposeActor();
        frogBt.disposeActor();
        pengiunBt.disposeActor();
        pigBt.disposeActor();
        mouseBt.disposeActor();
        deerBt.disposeActor();
        beerBt.disposeActor();


    }


    private void addListenersToButtons(){
        moveSpeedBt.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playClickSound();
                if(moveSpeedBt.increaseStatLevel()){
                    PlayerData.flushMoveSpeedLevel(moveSpeedBt.getStatLevel());
                    Hero.increaseMoveSpeed(1);
                }




            }


        });

        flameLgBt.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playClickSound();
               if(flameLgBt.increaseStatLevel()) {
                   PlayerData.flushLightTime(flameLgBt.getStatLevel());
                   DarkBackground.addTimeToDarkBackground(1);

               }


            }


        });



        luckBt.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playClickSound();
               if(luckBt.increaseStatLevel()){
                   LevelGenerator2.increaseLuck();
                   PlayerData.flushLuck(luckBt.getStatLevel());

               }


            }


        });


        shieldBt.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playClickSound();
                if(shieldBt.increaseStatLevel()){
                   // LevelGenerator2.increaseLuck();
                    Shield.increaseShieldTime();
                    ShieldTime.increaseStartWidth();

                    PlayerData.flushShieldTimeLvl(shieldBt.getStatLevel());
                }


            }


        });



       addListenerToCharacterButton(catBt,0);
       addListenerToCharacterButton(rabbitButton,1);
       addListenerToCharacterButton(frogBt,2);
       addListenerToCharacterButton(pengiunBt,3);
       addListenerToCharacterButton(pigBt, 4);
       addListenerToCharacterButton(mouseBt,5);
       addListenerToCharacterButton(deerBt,6);
       addListenerToCharacterButton(beerBt,7);




    }


    private void addListenerToCharacterButton(final CharacterButton button, final int value){
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playClickSound();

                if(!button.isSelected && button.isBought){
                    selectedButton.isSelected = false;
                    button.isSelected = true;
                    selectedButton = button;
                    PlayerData.flushPlayerSelectedInteger(value);

                    Hero.setAnimal(value);
                    BaseScreen.paused = false;
                    BaseScreen.paused = true;

                }


                if(!button.isBought){
                    button.buyCharacter();
                }



            }


        });
    }



    public void playClickSound(){
        if(BaseScreen.sounOn){
            pressSound.play();
        }


    }

}
