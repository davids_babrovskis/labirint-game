package com.davegame.labirint.objects;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 07/02/2020.
 */

public class TitleImage extends GameObject{
    private TextureRegion heroImg;
    private TextureRegion bgMaze, bgMaze2;

    private int moveX=1, moveX2=1;

    public TitleImage(GameAssets mng){

        heroImg = new TextureRegion( mng.getTitleImage());
        bgMaze = new TextureRegion( mng.getBgImage());
        bgMaze2 = bgMaze;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        moveX-=1;

        if(moveX <-1920){
            moveX=0;
        }

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //super.draw(batch, parentAlpha);
        batch.draw(bgMaze,moveX,400);
        batch.draw(bgMaze,moveX+1920,400);
        batch.draw(heroImg,0,0);
    }
}
