package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.GameObject;



/**
 * Created by Dave on 20/01/2020.
 */

public class Torch extends GameObject {
    private GameObject gmObject;

    private TextureRegion torch;


    private TextureRegion[] torchFrames;


    AnimatedObject animation;

    private float animationDelta;

    private static int torchRegW;
    private static int torchRegH;


    private static float toRemoveX;
    private static float toRemoveY;


    private static int beginingW, beginingH;

    private ColorAction reColorAction;

    public Torch(GameObject gmObject, TextureAtlas atlas){
        torchFrames = new TextureRegion[3];

        torchFrames[0]= atlas.findRegion("fire");
        torchFrames[1]= atlas.findRegion("fire2");
        torchFrames[2]= atlas.findRegion("fire3");

        animation = new AnimatedObject(torchFrames);
        animation.createAnimation(0.1f, Animation.PlayMode.LOOP);


        super.setTextureRegion(atlas.findRegion("fireStick"));
        this.gmObject = gmObject;
        super.setPosition(gmObject.getX() - 10,gmObject.getY() + gmObject.getHeight() *0.3f);
        torch = atlas.findRegion("fire");


        torchRegW = torch.getRegionWidth();
        torchRegH= torch.getRegionHeight();

        beginingH = torchRegH;
        beginingW = torchRegW;

        toRemoveX = torchRegW / 26;
        toRemoveY = torchRegH /26;

         reColorAction = Actions.color(new Color(0, 0, 1, 1f), 0.01f);

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(torch,this.getX() - (torchRegW /2) +7 , this.getY() + this.getHeight()-10,torchRegW,torchRegH);

        torch.setRegion(animation.animation.getKeyFrame(animationDelta));

    }

    public static void RemoveWidthHeight(){
        torchRegW -= toRemoveX;
        torchRegW -= toRemoveY;


    }





    public static void addWandH(){

        int newTorchSizeW =(int)( torchRegW + (toRemoveX *20));
        int newTorchSizeH = (int)( torchRegH + (toRemoveY *20));

        if(newTorchSizeW <  beginingW){
            torchRegW = newTorchSizeW;
        }else{
            torchRegW = beginingW;

        }

        if(newTorchSizeH < beginingH){
            torchRegH = newTorchSizeH;

        }else{
            torchRegH = beginingH;

        }
    }

    public  void restoreTorchSize(){
        //restore torch defoult color
        super.setColor(Color.WHITE);
        //Reset torch color



        torchRegH = beginingH;
        torchRegW = beginingW;
    }


    public static void removeWandH(){
        int newTorchSizeW =(int)( torchRegW - (toRemoveX ));
        int newTorchSizeH = (int)( torchRegH - (toRemoveY ));

        if(newTorchSizeW >  0){
            torchRegW = newTorchSizeW;
        }else{
            torchRegW = 0;

        }

        if(newTorchSizeH > 0){
            torchRegH = newTorchSizeH;

        }else{
            torchRegH = 0;

        }




    }

    @Override
    public void act(float delta) {



        animationDelta += delta;
        super.act(delta);
        super.setPosition(gmObject.getX( ) - 10,gmObject.getY()+ gmObject.getHeight() *0.3f);


    }


}
