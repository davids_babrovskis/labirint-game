package com.davegame.labirint.objects;

import com.davegame.labirint.interfaces.InterfaceChangeBestScore;
import com.davegame.labirint.interfaces.InterfaceChangeCoinValue;

/**
 * Created by Dave on 08/02/2020.
 */

public class GameData implements InterfaceChangeCoinValue,InterfaceChangeBestScore {
    private static GameData gameData=null;
    private int coinCount=0;
    private int level=0;
    private int bestScore;

    public int getBestScore() {
        return bestScore;
    }

    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }

    @Override
    public void bestScoreChanged(int value) {
        if(value>bestScore){
            bestScore = value;
        }
        this.setBestScore(bestScore);
    }

    private GameData(){
        SignalHandler.getInstance().subscribeToChangeCoin(this);
        SignalHandler.getInstance().subscribeToBestScoreChange(this);
    }


    public static GameData getInstance(){
        if(gameData == null){
            gameData = new GameData();
        }

        return  gameData;

    }

    public int getCoinCount() {
        return coinCount;
    }



    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    @Override
    public void increaseCoin(int value) {
        coinCount +=value;
    }

    @Override
    public void setCoin(int value) {
        this.coinCount = value;
    }

}
