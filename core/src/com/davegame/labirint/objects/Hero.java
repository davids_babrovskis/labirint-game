package com.davegame.labirint.objects;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.compression.lzma.Base;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.LevelGenerator2;
import com.davegame.labirint.base.ManyAnimationObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

import java.util.ArrayList;

/**
 * Created by Dave on 09/09/2019.
 */

public class Hero extends ManyAnimationObject {
    public static Vector2  move;
    public static float moveSpeed=1;
    public static boolean canMove = true;

    private boolean useGamepad;

    private static boolean noLight;

   // private static  TextureRegion heroTexture, rabbitTexture, frogTexture, penguinTexture, pigTexture, mouseTexture, deerTexture, beerTexture;


    private static int padSpeedDecrese= 34;
    private static int ghosSpeedDecrese =70;


    public static boolean canRemoveFlame = true;
    private float canRemoveFlameTime=2f;

    private float damageCandleTime=0;


    private static int tempSpeed=0;


    public static boolean mustLoadNewLevel = false;
    private LevelExit exit;



    private AnimatedObject shieldAnim;
    private float shieldAnimTime;


    private static boolean shieldEnabled=false;
    private static float shieldWorkTime;



    private ArrayList<Wall> wallList;
    private ArrayList<Ghost> ghostList;



    private Sound coinSound;
    private Sound torchSound;
    private Sound shieldSound;
    private Sound ghostSound;

    public static String beerName="beer", animName = "cat", rabbitName="rabbit", frogName="frog",penguin="penguin",pigName ="pig",mouseName="mouse",deerName="deer";
    private static  String  current;


    public void setWallAndGhost(ArrayList<Wall> wallList, ArrayList<Ghost> ghostList){
        this.wallList = wallList;
        this.ghostList = ghostList;
    }



    public static void resetPadSpeedDecrease(){
        tempSpeed =34;
        padSpeedDecrese = 34;
        ghosSpeedDecrese =70;

    }

    public static boolean returnShieldEnabled(){
        return shieldEnabled;
    }

    public static void setShieldVisible(){
        if(shieldEnabled==false){
            shieldEnabled =true;
        }

    }

    public static void setNoLightToFalse(){
        noLight = false;
    }

    public static void setLighToOf(){
        noLight = true;

    }

    public static String getCurentAnimalName(){
        return current;

    }


    public static void setAnimal(int num){


        switch (num){
            case 0: current = animName; break;
            case 1: current = rabbitName;break;
            case 2: current = frogName;break;
            case 3:current = penguin;break;
            case 4: current = pigName;break;
            case 5: current = mouseName; break;
            case 6: current = deerName;break;
            case 7: current = beerName;break;
        }
    }




    public static void increaseMoveSpeed(int value){
        if(tempSpeed -value >= 15){
            tempSpeed = (tempSpeed -value);
           // BaseScreen.writeLog("Pad speed decrease is " + padSpeedDecrese);

        }

        if(padSpeedDecrese != ghosSpeedDecrese && canRemoveFlame == true){

            padSpeedDecrese = tempSpeed;
        }


        //BaseScreen.writeLog("move speed decrease " + tempSpeed);

        ghosSpeedDecrese = (ghosSpeedDecrese -value);
    }






    public static boolean returnLightB(){

        return  noLight;
    }



    public Hero(LevelExit exit, GameAssets assets){


        this.coinSound = assets.getCoinTaken();
        this.torchSound = assets.getTorchSound();
        this.ghostSound = assets.getGhostSound();
        this.shieldSound = assets.getShieldSound();

        TextureAtlas  atlas= assets.getTextureAtlas();

        TextureRegion[] heroStayFrames = new TextureRegion[3];
        heroStayFrames[0] = atlas.findRegion("Cat");
        //heroTexture = heroStayFrames[0];
        heroStayFrames[1] = atlas.findRegion("Cat2");
        heroStayFrames[2] = atlas.findRegion("Cat3");


        TextureRegion[] heroWalkFrames = new TextureRegion[3];
        heroWalkFrames[0] = atlas.findRegion("CatW1");
        heroWalkFrames[1] = atlas.findRegion("CatW2");
        heroWalkFrames[2] = atlas.findRegion("CatW3");



        TextureRegion[] heroStunned = new TextureRegion[2];
        heroStunned[0] = atlas.findRegion("CatS1");
        heroStunned[1] = atlas.findRegion("CatS2");


        TextureRegion[] rabbitStayFrames = new TextureRegion[3];
        rabbitStayFrames[0] = atlas.findRegion("rabbit");
        //rabbitTexture = rabbitStayFrames[0];
        rabbitStayFrames[1] = atlas.findRegion("rabbit2");
        rabbitStayFrames[2] = atlas.findRegion("rabbit3");

        TextureRegion[] rabbitWalkFrames= new TextureRegion[3];
        rabbitWalkFrames[0] = atlas.findRegion("rabbitW1");
        rabbitWalkFrames[1] = atlas.findRegion("rabbitW2");
        rabbitWalkFrames[2] = atlas.findRegion("rabbitW3");


        TextureRegion[] rabbitStunned= new TextureRegion[2];
        rabbitStunned[0] = atlas.findRegion("rabbitS1");
        rabbitStunned[1] = atlas.findRegion("rabbitS2");




        TextureRegion[] frogSty = new TextureRegion[3];
        frogSty[0] = atlas.findRegion("frog");
        //frogTexture=frogSty[0];
        frogSty[1] = atlas.findRegion("frog2");
        frogSty[2] = atlas.findRegion("frog3");


        TextureRegion[] frogWalk = new TextureRegion[3];
        frogWalk[0] = atlas.findRegion("frogWalk1");
        frogWalk[1] = atlas.findRegion("frogWalk2");
        frogWalk[2] = atlas.findRegion("frogWalk3");



        TextureRegion[] frogStunned = new TextureRegion[2];
        frogStunned[0] =atlas.findRegion("frogS1");
        frogStunned[1] = atlas.findRegion("frogS2");




        TextureRegion[] penguinStay = new TextureRegion[3];
        penguinStay[0] = atlas.findRegion("penguin");
        //penguinTexture = penguinStay[0];
        penguinStay[1] = atlas.findRegion("penguin2");
        penguinStay[2] = atlas.findRegion("penguin3");


        TextureRegion[] penguinWalk = new TextureRegion[3];
        penguinWalk[0] = atlas.findRegion("penguinW1");
        penguinWalk[1] = atlas.findRegion("penguinW2");
        penguinWalk[2] = atlas.findRegion("penguinW3");



        TextureRegion[] penguinStunned = new TextureRegion[2];
        penguinStunned[0] = atlas.findRegion("penguinS1");
        penguinStunned[1] = atlas.findRegion("penguinS2");




        //Prepere pig textures
        TextureRegion[] pigStay = new TextureRegion[3];
        pigStay[0] = atlas.findRegion("Pig");
        //pigTexture = pigStay[0];
        pigStay[1] = atlas.findRegion("Pig2");
        pigStay[2] = atlas.findRegion("Pig3");


        TextureRegion[] pigWalk = new TextureRegion[3];
        pigWalk[0] = atlas.findRegion("PigW1");
        pigWalk[1] = atlas.findRegion("PigW2");
        pigWalk[2] = atlas.findRegion("PigW3");



        TextureRegion[] pigStunned = new TextureRegion[2];
        pigStunned[0] =atlas.findRegion("PigS1");
        pigStunned[1] = atlas.findRegion("PigS2");


        //Prepere mouse textures
        TextureRegion[] mouseStay = new TextureRegion[3];
        mouseStay[0] = atlas.findRegion("Mouse");
        //mouseTexture = mouseStay[0];
        mouseStay[1] = atlas.findRegion("Mouse2");
        mouseStay[2] = atlas.findRegion("Mouse3");


        TextureRegion[] mouseWalk = new TextureRegion[3];
        mouseWalk[0] = atlas.findRegion("MouseW1");
        mouseWalk[1] = atlas.findRegion("MouseW2");
        mouseWalk[2] = atlas.findRegion("MouseW3");



        TextureRegion[] mouseStunned = new TextureRegion[2];
        mouseStunned[0] = atlas.findRegion("MouseS1");
        mouseStunned[1] = atlas.findRegion("MouseS2");


        //Prepere deer textures
        TextureRegion[] deerStay = new TextureRegion[3];
        deerStay[0] = atlas.findRegion("deer1");
        //deerTexture = deerStay[0];
        deerStay[1] = atlas.findRegion("deer2");
        deerStay[2] = atlas.findRegion("deer3");


        TextureRegion[] deerWalk = new TextureRegion[3];
        deerWalk[0] = atlas.findRegion("deerW1");
        deerWalk[1] = atlas.findRegion("deerW2");
        deerWalk[2] = atlas.findRegion("deerW3");



        TextureRegion[] deerStunned = new TextureRegion[2];
        deerStunned[0] =atlas.findRegion("deerS1");
        deerStunned[1] = atlas.findRegion("deerS2");



        //Prepere beer textures
        TextureRegion[] beerStay = new TextureRegion[3];
        beerStay[0] = atlas.findRegion("beer1");
        //beerTexture = beerStay[0];
        beerStay[1] = atlas.findRegion("beer2");
        beerStay[2] = atlas.findRegion("beer3");


        TextureRegion[] beerWalk = new TextureRegion[3];
        beerWalk[0] = atlas.findRegion("beerW1");
        beerWalk[1] = atlas.findRegion("beerW2");
        beerWalk[2] = atlas.findRegion("beerW3");



        TextureRegion[] beerStunned = new TextureRegion[2];
        beerStunned[0] = atlas.findRegion("beerS1");
        beerStunned[1] = atlas.findRegion("beerS2");





        createAnimation(beerStay,beerWalk,beerStunned,beerName);
        createAnimation(deerStay,deerWalk,deerStunned,deerName);
        createAnimation(mouseStay,mouseWalk,mouseStunned, mouseName);
        createAnimation(pigStay,pigWalk,pigStunned, pigName );
        createAnimation(penguinStay,penguinWalk,penguinStunned, penguin );
        createAnimation(heroStayFrames,heroWalkFrames,heroStunned, animName );
        createAnimation(rabbitStayFrames,rabbitWalkFrames,rabbitStunned, rabbitName );
        createAnimation(frogSty,frogWalk,frogStunned, frogName );

        if(PlayerData.getPlayerInteger() !=0){
            setAnimal(PlayerData.getPlayerInteger());
        }else{
            setAnimal(0);
        }


        //current = rabbitName;


        TextureRegion[] shieldFrames = new TextureRegion[3];
        shieldFrames[0] = atlas.findRegion("heroShield1");
        shieldFrames[1] = atlas.findRegion("heroShield2");
        shieldFrames[2] = atlas.findRegion("heroShield3");

        shieldAnim = new AnimatedObject(shieldFrames);
        shieldAnim.createAnimation(0.5f, Animation.PlayMode.LOOP_PINGPONG);



        //store

        super.setActiveAnimation("staycat");
        tempSpeed = padSpeedDecrese;
       // super.setTextureRegion(new TextureRegion(new Texture("Cat.png")));
        super.setBoundingRectangle();
        this.exit = exit;
        super.setOrigin(super.getWidth()/2, super.getHeight()/2);
        move =new Vector2(0,0);
        super.setPosition(200, 300);

        this.wallList = wallList;

        useGamepad = false;

        noLight= false;

    }


    private void  createAnimation(TextureRegion[] tex1, TextureRegion[] tex2, TextureRegion[] tex3,String animalName){
        Animation<TextureRegion> stayAnim = new Animation<TextureRegion>(0.2f, tex1);
        stayAnim.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        Animation<TextureRegion> walkAnim = new Animation<TextureRegion>(0.2f, tex2);
        walkAnim.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);


        Animation<TextureRegion> stunnedAnim = new Animation<TextureRegion>(0.2f,tex3);
        stunnedAnim.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        storeAnimation("stay" + animalName, stayAnim);
        storeAnimation("walk" + animalName, walkAnim);
        storeAnimation("stunned" + animalName, stunnedAnim);





    }



    public static void MoveLeft(){
        if(canMove){
            move.x -= moveSpeed;
        }


    }


    public static void MoveRight(){
        if(canMove){
            move.x += moveSpeed;
        }
    }
    public static void MoveUp(){
        if(canMove){
            move.y += moveSpeed;
        }


    }

    public static void MoveDown(){
        if(canMove){

            move.y -= moveSpeed;
        }


    }

    public static void StopMovingX(){
        move.x = 0;

    }


    public static void StopMovingY(){
        move.y = 0;

    }

    public  float returnPositionX(){

        return this.getX();
    }


    public  float returnPositionY(){

        return this.getY();
    }






    @Override
    public void act(float delta) {


        super.act(delta);
        super.setPosition(super.getX() + move.x , super.getY() +move.y);
        shieldAnimTime+=delta;

        // Chechk if this overlaps wall
        for(int i=0; i<wallList.size(); i++){
            if(this.overlaps(wallList.get(i),true));

        }

        //Check coin list
        for(int i=0; i<LevelGenerator2.coinList.size(); i++){
            if(this.overlaps(LevelGenerator2.coinList.get(i),false)){
                LevelGenerator2.coinToDelete =LevelGenerator2.coinList.get(i);
                SignalHandler.getInstance().dispatchIncreaseCoins(1);
                playSound(coinSound);
            }

        }



        for(int i=0; i<LevelGenerator2.torchBonusList.size(); i++){
            if(this.overlaps(LevelGenerator2.torchBonusList.get(i),false)){
                LevelGenerator2.torchToDelete = LevelGenerator2.torchBonusList.get(i);
                LevelGenerator2.torchBonusList.get(i).increaseTorchSize();
                DarkBackground.restoreBackgroundSize();
                playSound(torchSound);


            }

        }




        LevelGenerator2.deleteTorchBonuss();
        LevelGenerator2.deleteCoinFromList();


        for(int i=0; i<LevelGenerator2.shieldList.size(); i++){
            if(this.overlaps(LevelGenerator2.shieldList.get(i),false)){
                LevelGenerator2.shieldToDelete = LevelGenerator2.shieldList.get(i);
                setShieldVisible();
                shieldWorkTime=0;
                 playSound(shieldSound);

                damageCandleTime = canRemoveFlameTime +1;
                ShieldTime.resetTimeWidth();

            }

        }


        LevelGenerator2.deleteShieldFromList();



        shieldAnimTime+=delta;



        if(shieldEnabled && mustLoadNewLevel == false){
            shieldWorkTime+=delta;

            if(shieldWorkTime >= Shield.shieldWorkTime){
                shieldEnabled = false;
                shieldWorkTime =0;
                ShieldTime.resetTimeWidth();

            }

        }


        for(int i=0; i<ghostList.size(); i++){
            if(this.overlaps(ghostList.get(i),false)){
                if(canRemoveFlame == true && shieldEnabled == false){
                    playSound(ghostSound);
                    setActiveAnimation("stunned"+current);
                    canRemoveFlame = false;
                    moveSpeed= moveSpeed /2;
                    tempSpeed = padSpeedDecrese;
                    padSpeedDecrese =ghosSpeedDecrese;
                    super.setColor(Color.CYAN);
                    ghostList.get(i).damageCandle();
                    DarkBackground.removeBackgroundSize();

                    break;

                }


            }

        }


        if(this.overlaps(exit,false)){
            mustLoadNewLevel= true;
        }


        if(GamePad.isVisible){
            move.x = GamePad.padMovePositionX /padSpeedDecrese;
            move.y = GamePad.padMovePositionY/padSpeedDecrese;

            if(padSpeedDecrese != ghosSpeedDecrese){
                if(!getAnimationName().equals("walk"+ current )){
                    setActiveAnimation("walk" + current );

                }

            }


            if(useGamepad!=true){
                useGamepad = true;
            }


        }else{

            if(!getAnimationName().equals( "stay" + current ) && !getAnimationName().equals("stunned" + current )){

                setActiveAnimation("stay" + current );
            }

            if(useGamepad ==true){
                useGamepad=false;
                move.x = 0;
                move.y=0;
            }


        }


        //Chechk if candle can be damaged
        if(canRemoveFlame ==false){
            damageCandleTime+=delta;

            if(damageCandleTime >= canRemoveFlameTime){
                padSpeedDecrese =tempSpeed;
                moveSpeed =6;
                setActiveAnimation("stay" + current);
                canRemoveFlame = true;
                super.setColor(Color.WHITE);
                damageCandleTime =0;

            }
        }


    }
    public void resetHeroColor(){
        //if(padSpeedDecrese == ghosSpeedDecrese){
            //super.setColor(Color.WHITE);
           // padSpeedDecrese = tempSpeed;
        //}

        canRemoveFlame = false;
        damageCandleTime = canRemoveFlameTime+1f;
        padSpeedDecrese = tempSpeed;
        //setActiveAnimation("stay");


    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if(shieldEnabled){
            batch.draw(shieldAnim.animation.getKeyFrame(shieldAnimTime),super.getX()-60, super.getY()-35);

        }

    }


    @Override
    public void disposeActor() {
        super.disposeActor();

    }


    public void playSound(Sound s){
        if(BaseScreen.sounOn){

            s.play();
        }

    }

    public void resetHeroPosition(){
       // BaseScreen.writeLog("newHeroPosition.X"+LevelGenerator2.newHeroPosition.x+"newHeroPosition.Y"+ LevelGenerator2.newHeroPosition.y);
        this.setPosition(LevelGenerator2.newHeroPosition.y *200,LevelGenerator2.newHeroPosition.x * 200);
    }





}
