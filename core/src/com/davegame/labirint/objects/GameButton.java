package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Dave on 23/01/2020.
 */

public class GameButton extends ImageButton {
    public GameButton(Drawable imageUp, Drawable imageDown, Drawable imageChecked, float positionX, float positionY) {
        super(imageUp, imageDown, imageChecked);
        this.setPosition(positionX, positionY);
        this.setOrigin(this.getWidth() /2, this.getHeight() /2);
        this.setTransform(true);


        this.addListener(new ClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                GameButton.super.setScale(1.1f);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                GameButton.super.setScale(1);
            }

        });





    }



    public GameButton(Drawable imageUp, float positionX, float positionY) {
        super(imageUp);

        this.setOrigin(this.getWidth() /2, this.getHeight() /2);
        this.setPosition(positionX, positionY);
        this.setTransform(true);

        this.addListener(new ClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                GameButton.super.setScale(1.1f);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                GameButton.super.setScale(1f);
            }

        });


    }

    public void disposeButton(){
        this.addAction(Actions.removeActor());

    }

    public GameButton( float positionX, float positionY, TextureRegion ballImage) {
        super(new TextureRegionDrawable( new TextureRegion(new Texture("BuyButton.png"))));
        this.setPosition(positionX, positionY);
        this.setOrigin(this.getWidth() /2, this.getHeight() /2);
        this.setTransform(true);


    }
}
