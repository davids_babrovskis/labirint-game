package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;

import java.util.Random;

/**
 * Created by Dave on 09/09/2019.
 */

public class Wall extends GameObject {

    Random rand;
    private static Color randomColor;
    private TextureAtlas atlas;

    static{
       /* grassWall = new TextureRegion[25];
        grassWall[0] = new TextureRegion(new Texture("Wall.png"));
        grassWall[1] = new TextureRegion(new Texture("Wall1.png"));
        grassWall[2] = new TextureRegion(new Texture("Wall2.png"));
        grassWall[3] = new TextureRegion(new Texture("Wall3.png"));
        grassWall[4] = new TextureRegion(new Texture("Wall4.png"));
        grassWall[5] = new TextureRegion(new Texture("Wall5.png"));
        grassWall[6] = new TextureRegion(new Texture("Wall6.png"));
        grassWall[7] = new TextureRegion(new Texture("Wall7.png"));
        grassWall[8] = new TextureRegion(new Texture("Wall8.png"));
        grassWall[9] = new TextureRegion(new Texture("Wall9.png"));
        grassWall[10] = new TextureRegion(new Texture("Wall10.png"));
        grassWall[11] = new TextureRegion(new Texture("Wall11.png"));
        grassWall[12] = new TextureRegion(new Texture("Wall12.png"));
        grassWall[13] = new TextureRegion(new Texture("Wall13.png"));
//        grassWall[14] = new TextureRegion(new Texture("Wall15.png"));

        grassWall[14] = new TextureRegion(new Texture("Wall14.png"));
        grassWall[15] = new TextureRegion(new Texture("Wall15.png"));
        grassWall[16] = new TextureRegion(new Texture("Wall16.png"));
        grassWall[17] = new TextureRegion(new Texture("Wall17.png"));
        grassWall[18] = new TextureRegion(new Texture("Wall18.png"));
        grassWall[19] = new TextureRegion(new Texture("Wall19.png"));
        grassWall[20] = new TextureRegion(new Texture("Wall20.png"));
        grassWall[21] = new TextureRegion(new Texture("Wall21.png"));
        grassWall[22] = new TextureRegion(new Texture("Wall22.png"));
        grassWall[23] = new TextureRegion(new Texture("Wall23.png"));
        grassWall[24] = new TextureRegion(new Texture("Wall24.png"));*/
    }

    public Wall(TextureAtlas atlas){
        this.atlas = atlas;
       super.setTextureRegion(getWallTexture(8));

        super.setBoundingRectangle();
        rand=new Random();

    }

    private TextureRegion getWallTexture( int numb){
        if(numb ==0){
            return atlas.findRegion("Wall");

        }else{
            return atlas.findRegion("Wall" + numb);
        }


    }

    public void setPosition(float x, float y){
        super.setPosition(x, y);

    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }


    public void dispose(){
        this.remove();

    }

    public static void createColor(){
        randomColor = new Color(MathUtils.random(0f,1f),MathUtils.random(0f,1f),MathUtils.random(0f,1f),1);

    }

    public void setWallTexture(int wallNum){

        //wallNum=24;
        //wallNum =0;
        super.setTextureRegion(this.getWallTexture(wallNum));


        if(wallNum ==5  || wallNum == 13){


            super.setColor(randomColor);
        }else{
            super.setColor(Color.WHITE);
        }


    }




}
