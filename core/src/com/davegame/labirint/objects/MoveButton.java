package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Dave on 25/09/2019.
 */

public class MoveButton extends ImageButton {
    private static TextureRegionDrawable buttonImage, buttNP;

    static{
        buttonImage = new TextureRegionDrawable(new TextureRegion(new Texture("Button.png")));
        buttNP = new TextureRegionDrawable(new TextureRegion(new Texture("ButtonnP.png")));
    }
    public MoveButton(float positionX, float positionY){
        super(buttNP,buttonImage);
        this.setPosition(positionX,positionY);
        this.setOrigin(this.getWidth() /2, this.getHeight() /2);

        this.setTransform(true);

    }

}
