package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 01/02/2020.
 */

public class CompanyLogo extends GameObject {

    public CompanyLogo(TextureRegion txt){
        super.setTextureRegion(txt);
        super.setOrigin(super.getWidth()/2, super.getHeight() /2);
        super.setPosition(BaseScreen.viewWidth *0.27f, BaseScreen.viewHeight * 0.5f);

        ScaleByAction scaleAction = Actions.scaleBy(1.05f, 1.05f, 3);

        super.addAction(scaleAction);


    }


    public void toggleVisible(){
        super.setVisible(!super.isVisible());
    }




}
