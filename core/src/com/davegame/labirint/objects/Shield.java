package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 03/02/2020.
 */

public class Shield extends GameObject {

    private AnimatedObject animObj;
    private float animTime;
    public static float shieldWorkTime =3;





    public static void increaseShieldTime(){

        shieldWorkTime+=0.5f;

    }



    public Shield(float positionX, float positionY, TextureAtlas atlas){

        TextureRegion[] shieldAnim = new TextureRegion[13];

        shieldAnim[0] = atlas.findRegion("shield");
        shieldAnim[1] = atlas.findRegion("shield1");
        shieldAnim[2] = atlas.findRegion("shield2");
        shieldAnim[3] = atlas.findRegion("shield3");
        shieldAnim[4] = atlas.findRegion("shield4");
        shieldAnim[5] = atlas.findRegion("shield5");
        shieldAnim[6] = atlas.findRegion("shield6");
        shieldAnim[7] = atlas.findRegion("shield7");
        shieldAnim[8] = atlas.findRegion("shield8");
        shieldAnim[9] = atlas.findRegion("shield9");
        shieldAnim[10] = atlas.findRegion("shield10");
        shieldAnim[11] = atlas.findRegion("shield11");
        shieldAnim[12] = atlas.findRegion("shield12");



        animObj = new AnimatedObject(shieldAnim);
        animObj.createAnimation(0.05f, Animation.PlayMode.LOOP);

        super.setTextureRegion(shieldAnim[0]);
        super.setPosition(positionX,positionY);
        super.setBoundingRectangle();



    }


    @Override
    public void act(float delta) {
        super.act(delta);
        animTime +=delta;

    }



    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        super.setTextureRegion(animObj.animation.getKeyFrame(animTime));
    }

    public void dispose(){
        super.remove();

    }
}
