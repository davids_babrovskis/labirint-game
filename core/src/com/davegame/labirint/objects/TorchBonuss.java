package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 22/01/2020.
 */

public class TorchBonuss extends GameObject {

    private AnimatedObject animation;


    private float torchTime;

    static{
        /*torchBTextures = new TextureRegion[5];

        torchBTextures[0] = new TextureRegion(new Texture("torchB1.png"));
        torchBTextures[1] = new TextureRegion(new Texture("torchB2.png"));
        torchBTextures[2] = new TextureRegion(new Texture("torchB3.png"));
        torchBTextures[3] = new TextureRegion(new Texture("torchB4.png"));
        torchBTextures[4] = new TextureRegion(new Texture("torchB5.png"));
        */
    }

    public TorchBonuss(float positionX, float positionY, TextureAtlas atlas){

        TextureRegion[] torchBTextures = new TextureRegion[5];

        torchBTextures[0] = atlas.findRegion("torchB1");
        torchBTextures[1] = atlas.findRegion("torchB2");
        torchBTextures[2] = atlas.findRegion("torchB3");
        torchBTextures[3] = atlas.findRegion("torchB4");
        torchBTextures[4] = atlas.findRegion("torchB5");



        animation = new AnimatedObject(torchBTextures);
        animation.createAnimation(0.25f, Animation.PlayMode.LOOP_PINGPONG);

        super.setTextureRegion( torchBTextures[1]);

        super.setOrigin(super.getWidth() /2,super.getHeight() /2 );
        super.setBoundingRectangle();
        super.setPosition(positionX, positionY);




    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        super.setTextureRegion(animation.animation.getKeyFrame(torchTime));
    }

    @Override
    public void act(float delta) {

        torchTime+=delta;
        super.act(delta);
    }

    public void dispose(){
        super.remove();

    }

    public void increaseTorchSize(){
        Torch.addWandH();

    }
}
