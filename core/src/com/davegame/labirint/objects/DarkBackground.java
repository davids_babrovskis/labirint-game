package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

import java.util.ArrayList;

/**
 * Created by Dave on 09/11/2019.
 */

public class DarkBackground extends GameObject {
    private static float beginingLightTime =100;

    private static float lightTime;



    private static TextureRegion lightRadiuss;
    private static ArrayList<TextureRegion> textureArray;
    private static int texturArraySize;

    private static float spriteTime;


    private static int spriteNumber=0;
    float changeTime;


    public static boolean canChange= true;



    public DarkBackground(GameAssets atlas1){


        TextureAtlas atlas =atlas1.getTextureAtlas();

        lightTime= beginingLightTime;

        textureArray = new ArrayList<TextureRegion>();
        textureArray.add(atlas.findRegion("lightRad"));
        textureArray.add(atlas.findRegion("lightRad1"));
        textureArray.add(atlas.findRegion("lightRad2"));
        textureArray.add(atlas.findRegion("lightRad3"));
        textureArray.add(atlas.findRegion("lightRad4"));
        textureArray.add(atlas.findRegion("lightRad5"));
        textureArray.add(atlas.findRegion("lightRad6"));
        textureArray.add(atlas.findRegion("lightRad7"));
        textureArray.add(atlas.findRegion("lightRad8"));
        textureArray.add(atlas.findRegion("lightRad9"));
        textureArray.add(atlas.findRegion("lightRad10"));
        textureArray.add(atlas.findRegion("lightRad11"));
        textureArray.add(atlas.findRegion("lightRad12"));
        textureArray.add(atlas.findRegion("lightRad13"));
        textureArray.add(atlas.findRegion("lightRad14"));

        texturArraySize = textureArray.size();


        super.setTextureRegion(new TextureRegion(atlas1.getDarkBGScreen()));

        lightRadiuss = textureArray.get(spriteNumber);

        spriteTime = lightTime / textureArray.size()-1;
    }



    public static void setTimeToDarkBg(int value){
        beginingLightTime =100 + (50 * value);


        lightTime= beginingLightTime;
        spriteTime = lightTime / texturArraySize;

    }

    public static void addTimeToDarkBackground( int value){
        beginingLightTime +=(50 * value);


        lightTime= beginingLightTime;
        spriteTime = lightTime /  texturArraySize;
    }

    public static void resetBgTime(){
        beginingLightTime = 100;
    }

    public static void resetDarkBackground(){
        lightTime =beginingLightTime;
        spriteNumber = 0;
        spriteTime = lightTime /  texturArraySize-1;
        lightRadiuss=textureArray.get(spriteNumber);


    }
    public static void restoreBackgroundSize(){


        if(spriteNumber ==1){
            spriteNumber -=1;

        } else if(spriteNumber ==2 ){
            spriteNumber -=2;


        }else if(spriteNumber !=0){
            spriteNumber -=3;

        }

         lightRadiuss= textureArray.get(spriteNumber);


    }

    public static void  toggleChange(){
        canChange = !canChange;
    }


    public static void removeBackgroundSize(){

        lightTime = (lightTime -10);
        spriteTime = lightTime / textureArray.size()-1;
        if(spriteTime <=0){
            spriteTime = 0.3f;
        }

        /*if(spriteNumber < textureArray.size()-1){
            spriteNumber+=1;

        }else{
            spriteNumber = textureArray.size()-1;
        }

        lightRadiuss= textureArray.get(spriteNumber);
    */
        }


        public static void resetSpriteNumber(){
            spriteNumber = 0;


        }

@Override
public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(lightRadiuss,BaseScreen.viewWidth *0.08f, BaseScreen.viewHeight*0.275f);
        }

@Override
    public void act(float delta) {
        super.act(delta);
        if(canChange){
            changeTime +=delta;
            if(spriteNumber ==textureArray.size() -1){
                changeTime = spriteTime;
            }
        }

        if(changeTime >=spriteTime){
            spriteNumber++;

            if(spriteNumber == textureArray.size()){
               Hero.setLighToOf();

            }

            if(Hero.returnLightB() == false){
                Torch.RemoveWidthHeight();
                lightRadiuss = textureArray.get(spriteNumber);
                changeTime=0;

            }

        }



    }

/*public void addActionToBackground(){
        super.addAction(Actions.color(com.badlogic.gdx.graphics.Color.BLACK, 2f));

    }
*/
}
