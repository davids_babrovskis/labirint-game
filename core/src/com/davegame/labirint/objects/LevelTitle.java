package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 17/01/2020.
 */

public class LevelTitle extends GameObject {
    BitmapFont levelTitleFont;
    public static int labirintNumber=1;

    public LevelTitle(GameAssets gameAssets){



        levelTitleFont = gameAssets.getFont5();
        levelTitleFont.setColor(Color.WHITE);
        levelTitleFont.getData().setScale(2);

        super.setPosition(BaseScreen.viewWidth *0.3f,BaseScreen.viewHeight *0.9f);

    }


    public static void increaseLevelNumber(){
        labirintNumber++;
    }

    public static void resetLevelNumber(){
        labirintNumber=1;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {

        levelTitleFont.draw(batch,"LABYRINTH "+ labirintNumber,super.getX(), super.getY());

    }
}
