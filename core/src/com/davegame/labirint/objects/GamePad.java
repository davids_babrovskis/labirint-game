package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 13/01/2020.
 */

public class GamePad extends GameObject {

    private TextureRegion pad;
    public static float padMovePositionX;
    public static float padMovePositionY;
    private float addedValueX;
    private float addedValueY;

    public  static boolean isVisible;
    static{

        isVisible = false;
    }

    public GamePad(TextureAtlas atlas){

        pad =atlas.findRegion("Pad");

        super.setTextureRegion(atlas.findRegion("buttonPanel"));
        super.setBoundingCircle();

        super.setOrigin(super.getWidth()/2, super.getHeight()/2);


        //super.setVisible(false);

        hideGamePad();
    }


    public void hideGamePad(){

        padMovePositionX = 0;
        padMovePositionY = 0;

        isVisible =false;
        super.setVisible(false);
    }

    public void setGamePadVisible(float posX, float posY){

        //super.setVisible(true);

        padMovePositionX = 0;
        padMovePositionY = 0;

        if(super.isVisible()==false){
            super.setVisible(true);
            isVisible =true;
        }

        super.setPosition(posX- super.getWidth() /2, posY - super.getHeight() /2);
    }

    public void setPadMovePOsition(float x, float y){
        addedValueX = x - (this.getX() + pad.getRegionWidth() /2 +90);
        //MathUtils.clamp(addedValueX,-250,250);
       //if(addedValueX >-90 && addedValueX <90){
        addedValueY = y- (this.getY() + pad.getRegionHeight() /2 +90);
        //if(addedValueX > -100 && addedValueX<100 ){

        padMovePositionX=MathUtils.clamp(addedValueX,-120,120);

        padMovePositionY=MathUtils.clamp(addedValueY,-150,150);


       // BaseScreen.writeLog("Pad position X " + padMovePositionX + "Pad position Y " + padMovePositionY);


        //MathUtils.clamp(addedValueY,-250,250);




    }


    public void setPadMovePositionToZero(){
        padMovePositionX=0;
        padMovePositionY=0;

    }


    @Override
    public void draw(Batch batch, float parentAlpha) {


        super.draw(batch, parentAlpha);

        batch.draw(pad,  this.getX() + (pad.getRegionWidth() /2  + 40+padMovePositionX), this.getY() + (pad.getRegionHeight() /2 ) +40+ padMovePositionY, pad.getRegionWidth() /2, pad.getRegionHeight() /2, pad.getRegionWidth(),pad.getRegionWidth(), 1,1,getRotation());
    }
}
