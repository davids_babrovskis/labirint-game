package com.davegame.labirint.objects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.ManyAnimationObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 01/02/2020.
 */

public class GameOverAnim extends GameObject {

    private TextureRegion catTexture;
    private float positionX, positionY;



    private float animTime=0;

    private AnimatedObject currentAnim,animEat, animEatLoop;


    private TextureAtlas atlas;
    private  TextureRegion bestScImg;


    public static boolean mustShowBestScore=false;

    private Sound gameOverSound;


    public GameOverAnim(float positionX, float positionY, GameAssets assets){

        gameOverSound =assets.getGameOverSound();

        this.atlas =assets.getTextureAtlas();
        bestScImg = new TextureRegion(assets.getBestImg());



        catTexture =  atlas.findRegion("catGameOver");
        //evilGhost= new TextureRegion(new Texture("BigGhost.png"));
        TextureRegion evilGhost[]=  new TextureRegion[13];
        evilGhost[0] =atlas.findRegion("BigGhost");
        evilGhost[1] = atlas.findRegion("BigGhost1");
        evilGhost[2] = atlas.findRegion("BigGhost2");
        evilGhost[3] = atlas.findRegion("BigGhost3");
        evilGhost[4] = atlas.findRegion("BigGhost4");
        evilGhost[5] = atlas.findRegion("BigGhost5");
        evilGhost[6] = atlas.findRegion("BigGhost6");
        evilGhost[7] = atlas.findRegion("BigGhost7");
        evilGhost[8] = atlas.findRegion("BigGhost8");
        evilGhost[9] = atlas.findRegion("BigGhost9");
        evilGhost[10] = atlas.findRegion("BigGhost10");
        evilGhost[11] = atlas.findRegion("BigGhost11");
        evilGhost[12] = atlas.findRegion("BigGhost12");


        TextureRegion evilGhostEaten[] = new TextureRegion[4];
        evilGhostEaten[0] = atlas.findRegion("BigGhost13");
        evilGhostEaten[1] = atlas.findRegion("BigGhost14");
        evilGhostEaten[2] = atlas.findRegion("BigGhost15");
        evilGhostEaten[3] = atlas.findRegion("BigGhost16");


        /*evilGhost[13] = new TextureRegion(new Texture("BigGhost13.png"));
        evilGhost[14] = new TextureRegion(new Texture("BigGhost14.png"));
        evilGhost[15] = new TextureRegion(new Texture("BigGhost15.png"));
        evilGhost[16] = new TextureRegion(new Texture("BigGhost16.png"));
        */



        super.setTextureRegion(evilGhost[0]);
        super.setOrigin(super.getWidth() /2, super.getHeight()/2);
        super.setPosition(positionX- 100, positionY -100);

        this.positionX = positionX;
        this.positionY = positionY;

        /*Animation<TextureRegion> eatAnim = new Animation<TextureRegion>(0.1f,evilGhost);
        eatAnim.setPlayMode(Animation.PlayMode.NORMAL);


        Animation<TextureRegion> eatLoop = new Animation<TextureRegion>(0.1f,evilGhostEaten);
        eatLoop.setPlayMode(Animation.PlayMode.LOOP);*/



       // storeAnimation("eat", eatAnim);
       // storeAnimation("loop", eatLoop);


       // setActiveAnimation("eat");



        animEat = new AnimatedObject(evilGhost);
        animEat.createAnimation(0.2f, Animation.PlayMode.NORMAL);

        animEatLoop = new AnimatedObject(evilGhostEaten);
        animEatLoop.createAnimation(0.1f, Animation.PlayMode.LOOP_PINGPONG);

        currentAnim = animEat;
    }


    public void setHeroDeathAnim(TextureRegion reg){
        catTexture = reg;

    }


    @Override
    public void act(float delta) {
        super.act(delta);

        if(super.isVisible()){
            animTime+=delta;


            if(animTime >2.5f){
                if(currentAnim != animEatLoop){
                    if(BaseScreen.sounOn){
                        gameOverSound.play();

                    }
                    currentAnim =animEatLoop;

                }


            }



        }



    }

    public void togggleVisible(){
        super.setVisible(!super.isVisible());

        if(super.isVisible()){
            setDeathAnimal();
            animTime=0;
            if(currentAnim != animEat){
                currentAnim = animEat;
                DarkBackground.setTimeToDarkBg(PlayerData.getLightTime());
            }
        }
    }



    private void setDeathAnimal(){

        if(Hero.getCurentAnimalName() == Hero.animName){
            catTexture = atlas.findRegion("catGameOver");
        } else if(Hero.getCurentAnimalName() == Hero.rabbitName){
            catTexture = atlas.findRegion("rabbitGameOver");
        } else if(Hero.getCurentAnimalName() == Hero.penguin){
            catTexture = atlas.findRegion("penguinGO");
        }else if(Hero.getCurentAnimalName() == Hero.pigName){
            catTexture = atlas.findRegion("pigGO");
        }else if(Hero.getCurentAnimalName() == Hero.frogName){
            catTexture = atlas.findRegion("frogGO");
        }else if(Hero.getCurentAnimalName() == Hero.mouseName){
            catTexture = atlas.findRegion("MouseGO");
        }else if(Hero.getCurentAnimalName() == Hero.deerName){
            catTexture = atlas.findRegion("DeerGO");
        }else if(Hero.getCurentAnimalName() == Hero.beerName) {
            catTexture = atlas.findRegion("beerGO");


        }

        }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        super.setTextureRegion(currentAnim.animation.getKeyFrame(animTime));


        if(currentAnim != animEatLoop){
            batch.draw(catTexture,positionX+30, positionY);

        }
        if(mustShowBestScore){
            batch.draw(bestScImg,BaseScreen.viewWidth *0.5f,BaseScreen.viewHeight *0.45f);
        }





        super.draw(batch, parentAlpha);

        //super.setTextureRegion(gameOverAnim.animation.getKeyFrame(animTime));


    }

    @Override
    public void disposeActor() {
        super.disposeActor();

    }
}
