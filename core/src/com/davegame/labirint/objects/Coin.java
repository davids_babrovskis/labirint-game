package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.GameObject;

import java.util.Random;

/**
 * Created by Dave on 18/01/2020.
 */

public class Coin extends GameObject {


    private AnimatedObject anim;
    private float coinTime=0;
    private TextureRegion coinTexture[];

    static{


        /*coinTexture = new TextureRegion[9];

        coinTexture[0] = new TextureRegion(new Texture("coin.png"));
        coinTexture[1] = new TextureRegion(new Texture("coin1.png"));
        coinTexture[2] = new TextureRegion(new Texture("coin2.png"));
        coinTexture[3] = new TextureRegion(new Texture("coin3.png"));
        coinTexture[4] = new TextureRegion(new Texture("coin4.png"));
        coinTexture[5] = new TextureRegion(new Texture("coin5.png"));
        coinTexture[6] = new TextureRegion(new Texture("coin6.png"));
        coinTexture[7] = new TextureRegion(new Texture("coin7.png"));
        coinTexture[8] = coinTexture[0];*/


    }

    public Coin(float positionX, float positionY, TextureAtlas atlas){

        coinTexture =  new TextureRegion[9];
        coinTexture[0] = atlas.findRegion("coin");
        coinTexture[1] = atlas.findRegion("coin1");
        coinTexture[2] = atlas.findRegion("coin2");
        coinTexture[3] = atlas.findRegion("coin3");
        coinTexture[4] = atlas.findRegion("coin4");
        coinTexture[5] = atlas.findRegion("coin5");
        coinTexture[6] = atlas.findRegion("coin6");
        coinTexture[7] = atlas.findRegion("coin7");
        coinTexture[8] = coinTexture[0];


        anim = new AnimatedObject(coinTexture);
        anim.createAnimation(0.05f, Animation.PlayMode.LOOP);



        this.setTextureRegion(coinTexture[0]);
        this.setOrigin(this.getWidth() /2, this.getHeight() /2);

        this.setPosition(positionX, positionY);
        this.setBoundingRectangle();

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        coinTime+=delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        super.setTextureRegion(anim.animation.getKeyFrame(coinTime));
    }

    public void dispose(){
        super.remove();


    }


}
