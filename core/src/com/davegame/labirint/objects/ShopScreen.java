package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 28/01/2020.
 */

public class ShopScreen extends GameObject {
    private TextureRegion shopScreenTx;


    public ShopScreen(GameAssets assets){
        shopScreenTx = new TextureRegion(assets.getShopBG());
        super.setTextureRegion(shopScreenTx);
        super.setVisible(false);
        super.setPosition(0,0);

    }

    public void toggleVisible(){
        super.setVisible(!super.isVisible());
    }

}
