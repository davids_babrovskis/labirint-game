package com.davegame.labirint.objects;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.LevelGenerator2;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 18/01/2020.
 */

public class LabirintCompleteText {

    public Container<Label> labelContainer;
    Label.LabelStyle  textStyle;

    Label text;


    public Action scaleAction;

    BitmapFont font, font1;


    public LabirintCompleteText(GameAssets assets){
        font =assets.getFont3();

        font1 = assets.getFont4();

        textStyle = new Label.LabelStyle();

        textStyle.font = font;
        text = new Label("LABYRINTH COMPLETED" , textStyle);


        scaleAction = Actions.scaleBy(1.1f, 1.1f, 3);


        font.getData().setScale(1.2f);

        //super.setPosition(BaseScreen.viewWidth *0.2f,BaseScreen.viewHeight *0.7f);




        labelContainer = new Container<Label>(text);
        labelContainer.setTransform(true);
        labelContainer.setOrigin(labelContainer.getWidth() / 2, labelContainer.getHeight() / 2);
        labelContainer.setPosition(BaseScreen.viewWidth *0.5f,BaseScreen.viewHeight *0.8f);

        labelContainer.setVisible(false);
    }

    private void setFontColor(){
        textStyle.fontColor =generateRandColor();
    }

    private Color generateRandColor(){

        Color color = new Color(MathUtils.random(0f,1f),MathUtils.random(0f,1f),MathUtils.random(0f,1f),1);;


        return color;
    }


    public void hideLabel(){
        if(labelContainer.isVisible() == true){
            labelContainer.setVisible(false);

        }
    }


    public void showEndLevelLabel(){
        if(labelContainer.isVisible()== false){
            scaleAction.restart();
            if(textStyle.font != font){
                textStyle.font = font;
                text.setStyle(textStyle);
            }

            if(!text.getText().equals("LABYRINTH COMPLETED")){
                text.setText("LABYRINTH COMPLETED");
            }
            textStyle.font = font1;


            //ontColor();
            labelContainer.setScale(1.2f,1.2f);
            labelContainer.setVisible(true);


            labelContainer.addAction(scaleAction);
        }

    }



    public void showGameOverText(){
        if(labelContainer.isVisible()== false){

            textStyle.font = font1;
            text.setStyle(textStyle);



            scaleAction.restart();
            text.setText("GAME OVER");

            //setBest Score
            SignalHandler.getInstance().dispathBestScoreChanged(LevelTitle.labirintNumber);
            //PlayerData.flushBestScore(LevelTitle.labirintNumber);
            CoinCountText.saveCoinCount();

            LevelGenerator2.resetGhostChance();

            labelContainer.setScale(0.7f,0.7f);
            labelContainer.setVisible(true);


            labelContainer.addAction(scaleAction);


        }
    }

    public void dispose(){
        labelContainer.remove();
        scaleAction.reset();

    }




}
