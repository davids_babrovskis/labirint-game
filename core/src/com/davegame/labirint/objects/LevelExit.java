package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 15/01/2020.
 */

public class LevelExit extends GameObject {



    public LevelExit(float positionX, float positionY,TextureAtlas atlas){
        super.setTextureRegion(atlas.findRegion("exit"));
        super.setPosition(positionX,positionY);
        super.setBoundingRectangle();



    }

    public void setLevelExitPosition(float positionX, float positionY){
        super.setPosition(positionX,positionY);

    }


}
