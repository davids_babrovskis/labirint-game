package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 06/02/2020.
 */

public class CharacterButton extends GameObject {
    private BitmapFont priceFont;

    private String price;
    private String characterName;

    public boolean isBought;


    private TextureRegion animalImg;


    private TextureRegion selected, notBought;

    private int characterPrice;

    public boolean isSelected = false;


    private boolean changeColor=false;
    private float changeTime=0;

    private float addedWidthX=80;





    private TextureRegion coinImage;


    public CharacterButton(float positionX, float positionY, int characterPrice, String characterName, boolean isBought, TextureRegion animalImg, GameAssets manager){


        TextureAtlas atlas = manager.getTextureAtlas();


        selected = atlas.findRegion("btSelected");
        notBought = atlas.findRegion("privatNotB");

        priceFont = manager.getFont2();
        priceFont.getData().setScale(1f);


        coinImage = atlas.findRegion("coin");

        this.characterPrice = characterPrice;
        this.animalImg = animalImg;
        this.characterName = characterName;
        this.isBought = isBought;






        if(PlayerData.getStringBoolean(characterName) == true){
            this.isBought = true;
        }



        if(this.isBought==true){
            price =this.characterName;
            addedWidthX = 20;
        }else{
            price ="" +characterPrice;

        }


        super.setPosition(positionX, positionY);

        super.setTextureRegion(atlas.findRegion("characterButton"));



    }






    public void buyCharacter(){
        int playerCoinCount = GameData.getInstance().getCoinCount();
        if(playerCoinCount >= characterPrice ){
            isBought = true;

            playerCoinCount = playerCoinCount-characterPrice;
            SignalHandler.getInstance().dispatchSetCoins(playerCoinCount);
            price = characterName;
            addedWidthX = 20;
            PlayerData.flushStringBoolean(characterName);

        }else{
            changeColor = true;
        }





    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        priceFont.draw(batch,price,super.getX()+addedWidthX,super.getY()-30);

        batch.draw(animalImg,super.getX()+50, super.getY()+40);

        if(isSelected && isBought){
            batch.draw(selected,super.getX()+10, super.getY()+10);

        }


        if(!isBought){
            batch.draw(coinImage,super.getX()+10, super.getY()-60);
            batch.draw(notBought,super.getX()+10, super.getY()+10);
        }

    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(changeColor == true){
            if(super.getColor() != Color.RED){
                super.setColor(Color.RED);
            }

            changeTime+=delta;


            if(changeTime>=0.1f){
                changeColor = false;
                super.setColor(Color.WHITE);
                changeTime=0;
            }

        }



    }
}
