package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;

/**
 * Created by Dave on 15/02/2020.
 */

public class GameTask extends GameObject {
    private Action scaleAction;
    public GameTask(GameAssets assets){
        super.setTextureRegion(new TextureRegion(assets.getTaskText()));
        super.setOrigin(super.getWidth() /2, super.getHeight() /2);
        super.setPosition(BaseScreen.viewWidth *0.25f, BaseScreen.viewHeight *0.8f);
        this.setScale(0.7f);

        scaleAction = Actions.scaleBy(0.4f, 0.4f, 3);
        scaleAction.restart();
        super.addAction(scaleAction);
    }

    public void scaleActionReset(){
        this.setScale(0.7f);
        scaleAction.restart();
        //super.addAction(scaleAction);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}
