package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.interfaces.InterfaceChangeBestScore;
import com.davegame.labirint.interfaces.InterfaceChangeCoinValue;

/**
 * Created by Dave on 08/02/2020.
 */

public class PlayerData implements InterfaceChangeCoinValue, InterfaceChangeBestScore{

    private int bestScore;
    private int coinCount;

    private static int moveSpeedLevel;
    private static int lightTime;
    private static int luckLevel;
    private static int shieldTime;



    private static Preferences prefs;




    static{
        prefs = Gdx.app.getPreferences("labirint");

         moveSpeedLevel = getMoveSpeedLevel();
         lightTime = getLightTime();
         luckLevel = getLuck();



    }

    public PlayerData(){

        SignalHandler.getInstance().subscribeToChangeCoin(this);
        SignalHandler.getInstance().subscribeToBestScoreChange(this);

        coinCount = getCoinCount();
        bestScore = getBestScore();
       // BaseScreen.writeLog("Best score is " + bestScore);
        SignalHandler.getInstance().dispatchSetCoins(coinCount);
        SignalHandler.getInstance().dispathBestScoreChanged(bestScore);
       // BaseScreen.writeLog("Player data constructor was called");
    }


    public static void saveSound(boolean value){
        prefs.putBoolean("sound", value);
        prefs.flush();
    }

    public static boolean loadSoundBoolean(){
        return prefs.getBoolean("sound");
    }

    public static void clearAllPref(){
        prefs.clear();

    }

    public static void firstTimeLounched(boolean val){
        prefs.putBoolean("firstTime", val);
        prefs.flush();
    }

    public static boolean getFitstTime(){
        return  prefs.getBoolean("firstTime");
    }



    private  void  flushBestScore(int value){
        prefs.putInteger("bestScore", value);
        prefs.flush();
    }


    public  void flushCoinCount(int value){
        prefs.putInteger("coinCount", value);
        prefs.flush();
    }

    public static int getCoinCount(){
        return prefs.getInteger("coinCount");
    }

    private static int getBestScore(){

       return  prefs.getInteger("bestScore");

    }


    public static void flushMoveSpeedLevel(int value){
        prefs.putInteger("moveSpeedLvl", value);
        prefs.flush();
    }

    public static int getMoveSpeedLevel(){
        return  prefs.getInteger("moveSpeedLvl");

    }

    public static void flushLightTime(int value){
        prefs.putInteger("lightTime", value);
        prefs.flush();
    }
    public static int getLightTime(){
        return  prefs.getInteger("lightTime");
    }

    public static void flushLuck(int value){
        prefs.putInteger("luckLvl", value);
        prefs.flush();
    }
    public static int getLuck(){
        return prefs.getInteger("luckLvl");
    }

    public static void flushShieldTimeLvl(int value){
        prefs.putInteger("shieldLvl", value);
        prefs.flush();
    }
    public static int getShieldTime(){
        return prefs.getInteger("shieldLvl");
    }


    public static void flushStringBoolean(String name){
        prefs.putBoolean(name, true);
        prefs.flush();
    }

    public static boolean getStringBoolean(String name){
        return prefs.getBoolean(name);
    }

    public static void flushPlayerSelectedInteger(int value){
        prefs.putInteger("player", value);
        prefs.flush();

    }

    public static int getPlayerInteger(){
        return  prefs.getInteger("player");
    }





    @Override
    public void increaseCoin(int value) {

    }

    @Override
    public void setCoin(int value) {
        flushCoinCount(value);
        BaseScreen.writeLog("Coin was set");
    }


    @Override
    public void bestScoreChanged(int value) {
        if(value >this.bestScore){
            this.bestScore = value;
        }

        flushBestScore(this.bestScore);
    }
}
