package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;
import com.davegame.labirint.interfaces.InterfaceChangeCoinValue;

/**
 * Created by Dave on 19/01/2020.
 */

public class CoinCountText extends GameObject implements InterfaceChangeCoinValue {


    BitmapFont font;
    private SignalHandler signalHandler;

    private String coinText = "" + GameData.getInstance().getCoinCount();


    public CoinCountText(GameAssets assets){

        super.setTextureRegion(assets.getTextureAtlas().findRegion("coin"));
        super.setPosition(BaseScreen.viewWidth *0.1f, BaseScreen.viewHeight*0.9f);

        int coinCount = PlayerData.getCoinCount();
        coinText= coinCount +"";

        SignalHandler.getInstance().dispatchSetCoins(coinCount);
        this.font = assets.getFont1();
        this.font.setColor(Color.WHITE);
        this.font.getData().setScale(2);

        this.signalHandler=SignalHandler.getInstance();
        this.signalHandler.subscribeToChangeCoin(this);


    }




    public static void saveCoinCount(){
       // PlayerData.flushCoinCount(coinCount);
    }



    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        this.font.draw(batch,this.coinText, super.getX()+ super.getWidth()+30,super.getY()+ super.getHeight() *0.9f);


    }


    @Override
    public void increaseCoin(int value) {
        //GameData.getInstance().increaseCoinCount(value);
        this.coinText = "" + GameData.getInstance().getCoinCount();
    }

    @Override
    public void setCoin(int value) {
        this.coinText = ""+value;
    }
}
