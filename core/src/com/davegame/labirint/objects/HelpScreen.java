package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 08/02/2020.
 */

public class HelpScreen extends GameObject {

    TextureRegion helpImg;
     float  positinY;

    public HelpScreen(TextureRegion txRegion){
        helpImg = txRegion;
        positinY = BaseScreen.viewHeight * 0.2f;

    }


    @Override
    public void draw(Batch batch, float parentAlpha) {

        batch.draw(helpImg,super.getX(), positinY);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

    }
}
