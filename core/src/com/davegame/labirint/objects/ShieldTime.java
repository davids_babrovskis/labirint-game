package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 04/02/2020.
 */

public class ShieldTime extends GameObject {

    private TextureRegion shieldTimeTexture;

    private static float timeWidth;
    private static float beginingWidth;



    public ShieldTime(TextureAtlas atlas){

        super.setTextureRegion(atlas.findRegion("shield"));
        shieldTimeTexture = atlas.findRegion("shieldTime");
        timeWidth = shieldTimeTexture.getRegionWidth() *1.8f;
        beginingWidth = timeWidth;

        super.setPosition(BaseScreen.viewWidth *0.2f, BaseScreen.viewHeight *0.73f);

    }

    public static void increaseStartWidth(){
        timeWidth +=30;
        beginingWidth +=30;

    }

    public static void setTimeWidth(int value){
        timeWidth = beginingWidth +(value *30);
      //  BaseScreen.writeLog("Time width is " + timeWidth);

    }


    public void toggleVisible(){
        super.setVisible(!super.isVisible());

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(Hero.returnShieldEnabled() && timeWidth >0 && Hero.mustLoadNewLevel == false && BaseScreen.paused == false){

            timeWidth -=1;
        }
}


    public static void resetTimeWidth(){
        timeWidth =beginingWidth;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(Hero.returnShieldEnabled()){
            super.draw(batch, parentAlpha);
            batch.draw(shieldTimeTexture, super.getX()+100, super.getY()+25, timeWidth, shieldTimeTexture.getRegionHeight());

        }


    }
}
