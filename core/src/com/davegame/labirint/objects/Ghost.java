package com.davegame.labirint.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.davegame.labirint.base.AnimatedObject;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.LevelGenerator2;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Dave on 18/01/2020.
 */

public class Ghost extends GameObject {

    private int moveSpeed=3;
    private Random random;

    private Vector2 moveWay;

    AnimatedObject anim;

    private float animTime;

    private  TextureRegion[] textureToSet;

    private ArrayList<Wall> wallList;

    public Ghost(float positionX, float positinoY, TextureAtlas atlas, ArrayList<Wall> wallList){
        Random rand = new Random();
        this.wallList = wallList;

        int ghostType = rand.nextInt(8);
        //ghostType =7;


        switch (ghostType){
            case 0:
                TextureRegion[] ghostTexture = new TextureRegion[3];
                ghostTexture[0] = atlas.findRegion("ghost");
                ghostTexture[1] = atlas.findRegion("ghost2");
                ghostTexture[2] = atlas.findRegion("ghost3");
                textureToSet = ghostTexture;
                break;
            case 1:
                TextureRegion[] eyeTexture = new TextureRegion[5];
                eyeTexture[0] = atlas.findRegion("eye");
                eyeTexture[1] = atlas.findRegion("eye2");
                eyeTexture[2] = atlas.findRegion("eye3");
                eyeTexture[3] = atlas.findRegion("eye4");
                eyeTexture[4] = atlas.findRegion("eye5");
                textureToSet = eyeTexture;
                break;
            case 2:
                TextureRegion[] skullTexture = new TextureRegion[4];
                skullTexture[0] = atlas.findRegion("skull");
                skullTexture[1] = atlas.findRegion("skull2");
                skullTexture[2] = atlas.findRegion("skull3");
                skullTexture[3] = atlas.findRegion("skull4");
                textureToSet = skullTexture;
                break;
            case 3:
                TextureRegion pumkinText[] = new TextureRegion[4];
                pumkinText[0] = atlas.findRegion("pumkin");
                pumkinText[1] = atlas.findRegion("pumkin2");
                pumkinText[2] = atlas.findRegion("pumkin3");
                pumkinText[3] = atlas.findRegion("pumkin4");
                textureToSet = pumkinText;
                break;
            case 4:
                TextureRegion[] batTexture = new TextureRegion[5];

                batTexture[0] = atlas.findRegion("bat1");
                batTexture[1] = atlas.findRegion("bat2");
                batTexture[2] = atlas.findRegion("bat3");
                batTexture[3] = atlas.findRegion("bat4");
                batTexture[4] = atlas.findRegion("bat5");
                textureToSet = batTexture;break;
            case 5:

                TextureRegion[] hatTexture = new TextureRegion[3];
                hatTexture[0] = atlas.findRegion("hat");
                hatTexture[1] = atlas.findRegion("hat2");
                hatTexture[2] = atlas.findRegion("hat3");

                textureToSet = hatTexture;break;
            case 6:
                TextureRegion[] slimeTexture = new TextureRegion[5];
                slimeTexture[0] = atlas.findRegion("Slime");
                slimeTexture[1] = atlas.findRegion("Slime1");
                slimeTexture[2] = atlas.findRegion("Slime2");
                slimeTexture[3] = atlas.findRegion("Slime23");
                slimeTexture[4] = atlas.findRegion("Slime3");

                textureToSet = slimeTexture;
                break;
            case 7:
                TextureRegion[] spikeTexture = new TextureRegion[4];
                spikeTexture[0] = atlas.findRegion("spike1");
                spikeTexture[1] = atlas.findRegion("spike2");
                spikeTexture[2] = atlas.findRegion("spike3");
                spikeTexture[3] = atlas.findRegion("spike4");
                textureToSet = spikeTexture;
                break;
        }




        anim = new AnimatedObject(textureToSet);
        anim.createAnimation(0.1f, Animation.PlayMode.LOOP_PINGPONG);

        super.setTextureRegion(textureToSet[0]);
        super.setOrigin(super.getWidth()/2, super.getHeight() /2);
        super.setPosition(positionX, positinoY);
        super.setBoundingRectangle();

        random = new Random();
        moveWay = new Vector2();
        setMoveForce();

    }
    private void setMoveForce(){
        int moveType = createMovePosition();
        moveSpeed = random.nextInt(10)+1;

        //Move up
        if(moveType==1){
            moveWay.set(0,moveSpeed);


        }else if(moveType==2){
            //MoveDown
            moveWay.set(0,-moveSpeed);
        }else if(moveType==3){
            //MoveRight
            moveWay.set(moveSpeed,0);
        }else if(moveType==4){
            //MoveLeft
            moveWay.set(-moveSpeed,0);
        }


    }


    private int createMovePosition(){
        int movePosition= random.nextInt(4)+1;

        return  movePosition;
    }

    public void dispose(){
        super.remove();

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        super.setTextureRegion(anim.animation.getKeyFrame(animTime));
    }

    public void damageCandle(){
        Torch.removeWandH();

    }

    @Override
    public void act(float delta) {
        super.setPosition(this.getX() +moveWay.x, this.getY() +moveWay.y);

        animTime +=delta;

        for(int i=0; i<wallList.size(); i++){
            if(this.overlaps(wallList.get(i),true)){
                setMoveForce();
            }
        }

    }
}
