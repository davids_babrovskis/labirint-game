package com.davegame.labirint.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.davegame.labirint.base.GameObject;

/**
 * Created by Dave on 28/01/2020.
 */

public class StatButton extends GameObject {


    private BitmapFont levelFont;

    private TextureRegion coinImage;

    private int statPrice=5;
    private int statLevel=0;

    private boolean changeColor;

    private float changeTime=0;

    private int maxLevel;

    private String statusText;
    private String statusPrice;

    public StatButton(TextureRegion statTexture, float posX, float posY, int maxLevel, TextureAtlas atlas){

        coinImage = atlas.findRegion("coin");

        levelFont = new BitmapFont(Gdx.files.internal("fonts/gamefont2.fnt"),false);
        levelFont.setColor(Color.WHITE);
        levelFont.getData().setScale(1.5f);



        super.setTextureRegion(statTexture);
        super.setPosition(posX,posY);

        changeColor = false;

        this.maxLevel = maxLevel;

        statusText = "LV " + statLevel;
        statusPrice= " " + statPrice;



    }

    public void setStatPrice(int value){
        statPrice = value;
    }


    public void setStatLevel(int value){
        statLevel = value;
        statusText = "LV " + statLevel;

        if(statLevel !=0){
            statPrice = 5+( statLevel *5);
        }

        statusPrice= " " + statPrice;

        if(statLevel == maxLevel){

            statusPrice= "MAX";
        }

    }

    public int getStatLevel(){return statLevel;}


    public boolean increaseStatLevel(){
        int playerCoinCount = GameData.getInstance().getCoinCount();
        if(playerCoinCount >= statPrice && statLevel <maxLevel){
            playerCoinCount = playerCoinCount -statPrice;

            SignalHandler.getInstance().dispatchSetCoins(playerCoinCount);  //(playerCoinCount);
            //PlayerData.flushCoinCount(playerCoinCount);


            statLevel+=1;
            statPrice +=5;
            if(statLevel == maxLevel){
                statusPrice= "MAX";
            }else{
                statusPrice= " " + statPrice;
            }

            statusText = "LV " + statLevel;

            return true;

        }else{
            changeColor = true;
            return  false;
        }


    }

    @Override
    public void act(float delta) {
        super.act(delta);


        if(changeColor == true){
            if(super.getColor() != Color.RED){
                super.setColor(Color.RED);
            }

            changeTime+=delta;


            if(changeTime>=0.1f){
                changeColor = false;
                super.setColor(Color.WHITE);
                changeTime=0;
            }

        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        levelFont.draw(batch,statusText,super.getX()+20,super.getY()+super.getHeight() -10);
        levelFont.draw(batch,statusPrice,super.getX()+60,super.getY()-15);
        batch.draw(coinImage,super.getX()+10, super.getY()-60);



    }
}
