package com.davegame.labirint.interfaces;

/**
 * Created by Dave on 08/02/2020.
 */

public interface InterfaceChangeBestScore {
    public void bestScoreChanged(int value);


}
