package com.davegame.labirint;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.davegame.labirint.base.BaseScreen;
import com.davegame.labirint.base.GameObject;
import com.davegame.labirint.base.mazegenerator.GameAssets;
import com.davegame.labirint.objects.CompanyLogo;
import com.davegame.labirint.objects.DarkBackground;
import com.davegame.labirint.objects.DaveGame;
import com.davegame.labirint.objects.GameButton;
import com.davegame.labirint.objects.HelpScreen;
import com.davegame.labirint.objects.Hero;
import com.davegame.labirint.objects.PlayerData;
import com.davegame.labirint.objects.TitleImage;

/**
 * Created by Dave on 26/01/2020.
 */

public class MainMenu extends BaseScreen {
    GameButton startButton;
    GameButton exitButton;

    GameButton helpButton;
    GameButton backButton;

    GameObject gameTitle;
    GameObject companyS;


    HelpScreen helpScreen;


    private CompanyLogo companyLogo;

    private TitleImage titleImg;

    private Music mainTheme;
    private Sound buttonPress;


    private static boolean ShowCompany =true;
    private float companyTime;


    private TextureAtlas txAtlas;

    private GameButton soundButton;



    public MainMenu(DaveGame g) {
        super(g);

       // this.assetManager = assetManager;


    }



    @Override
    public void create() {

        if(super.game.gameAssets == null){
            //toogleVisibilityOf();
            super.game.gameAssets = new GameAssets();
            super.game.gameAssets.loadTextures();
            super.game.gameAssets.loadFonts();
            super.game.gameAssets.loadDarkBGScreen();
            super.game.gameAssets.loadShopBG();

            super.game.gameAssets.loadHelpScreen();
            super.game.gameAssets.loadBgImage();
            super.game.gameAssets.loadTitleImage();
            super.game.gameAssets.loadBestImg();
            super.game.gameAssets.loadMusic();
            super.game.gameAssets.loadSound();
            super.game.gameAssets.loadSoundBtIm();
            super.game.gameAssets.loadTaskText();

            super.game.gameAssets.manager.finishLoading();



        }
        txAtlas = super.game.gameAssets.getTextureAtlas();
        mainTheme = super.game.gameAssets.getMainTheme();
        this.buttonPress = super.game.gameAssets.getButtonClick();

        playMainTheme();
        mainTheme.setLooping(true);

        if(ShowCompany){
            companyLogo = new CompanyLogo(txAtlas.findRegion("daveGameLogo"));
            uiStage.addActor(companyLogo);

        }





        gameTitle = new GameObject();
        gameTitle.setTextureRegion(txAtlas.findRegion("GameTitle"));
        gameTitle.setPosition(super.viewWidth * 0.17f, super.viewHeight * 0.7f);
        gameTitle.setOrigin(gameTitle.getWidth() /2, gameTitle.getHeight() /2);


        companyS = new GameObject();
        companyS.setTextureRegion(txAtlas.findRegion("daveGsml"));
        companyS.setOrigin(companyS.getWidth() /2, companyS.getHeight() /2);
        companyS.setPosition(super.viewWidth * 0.35f, super.viewHeight * 0.24f);

       // addAnimation(gameTitle);

        TextureRegionDrawable buttonIm = new TextureRegionDrawable(txAtlas.findRegion("PlayButton"));
        startButton = new GameButton(buttonIm,viewWidth * 0.3f, viewHeight *0.5f);


        TextureRegionDrawable exBIm = new TextureRegionDrawable(txAtlas.findRegion("ExitButton2"));
        exitButton = new GameButton(exBIm,viewWidth * 0.3f, viewHeight * 0.31f);

        TextureRegionDrawable helpDataBt = new TextureRegionDrawable(txAtlas.findRegion("helpButton"));
        helpButton = new GameButton(helpDataBt,viewWidth * 0.3f, viewHeight * 0.41f);

        TextureRegionDrawable backDataBt = new TextureRegionDrawable(txAtlas.findRegion("backBt"));
        backButton = new GameButton(backDataBt,viewWidth * 0.3f, viewHeight * 0.1f);


        soundButton = new GameButton(new TextureRegionDrawable(new TextureRegion(super.game.gameAssets.getSoundOnImg())), null, new TextureRegionDrawable(new TextureRegion(super.game.gameAssets.getSoundOfImg())),super.viewWidth *0.85f, super.viewHeight * 0.2f);

        if(BaseScreen.sounOn == false){
            soundButton.toggle();
        }


        helpScreen = new HelpScreen(new TextureRegion( super.game.gameAssets.getHelpScreen()));


        titleImg = new TitleImage(super.game.gameAssets);

        mainStage.addActor(titleImg);

        uiStage.addActor(soundButton);

        uiStage.addActor(exitButton);
        uiStage.addActor(startButton);

        uiStage.addActor(companyS);

        mainStage.addActor(gameTitle);

        uiStage.addActor(helpButton);
        uiStage.addActor(helpScreen);

        uiStage.addActor(backButton);




        backButton.setVisible(false);
        helpScreen.setVisible(false);


        if(ShowCompany){
            toogleVisibilityOf();
        }

        addListenersToButtons();






    }
    private void toogleVisibilityOn(){
        soundButton.setVisible(true);
        titleImg.setVisible(true);
        companyS.setVisible(true);

        gameTitle.setVisible(true);
        startButton.setVisible(true);
        exitButton.setVisible(true);

        helpButton.setVisible(true);

        backButton.setVisible(false);
        helpScreen.setVisible(false);

    }

    private void toogleVisibilityOf(){
        soundButton.setVisible(false);
        companyS.setVisible(false);
        titleImg.setVisible(false);
        gameTitle.setVisible(false);
        startButton.setVisible(false);
        exitButton.setVisible(false);

        helpButton.setVisible(false);
        backButton.setVisible(false);

        helpScreen.setVisible(false);
    }


    private void setVisibilityOffForHelpButton(){
        startButton.setVisible(false);
        exitButton.setVisible(false);
        soundButton.setVisible(false);

        helpButton.setVisible(false);
        gameTitle.setVisible(false);
        companyS.setVisible(false);

        backButton.setVisible(true);
        helpScreen.setVisible(true);
    }

    private void setVisibilityOnForHelpButton(){
        companyS.setVisible(true);
        gameTitle.setVisible(true);
        startButton.setVisible(true);
        exitButton.setVisible(true);
        soundButton.setVisible(true);

        helpButton.setVisible(true);

        backButton.setVisible(false);
        helpScreen.setVisible(false);
    }


    @Override
    public void render(float delta) {
            super.render(delta);



    }

    @Override
    public void update(float delta) {
        if(ShowCompany ){
            companyTime +=delta;


            if(companyTime > 4f){
                ShowCompany = false;
                companyLogo.toggleVisible();
                toogleVisibilityOn();

            }
           // if(companyTime >=5f){

           // }

        }




        //Recolor game bacground

    }


    @Override
    public void show() {
        super.show();

        //super.loadGameData();
    }


    @Override
    public void dispose() {


        super.dispose();
        startButton.disposeButton();
        exitButton.disposeButton();

        gameTitle.disposeActor();
        titleImg.disposeActor();
        helpButton.disposeButton();
        companyS.disposeActor();
        helpScreen.disposeActor();
        backButton.disposeButton();


        if(companyLogo != null){
            companyLogo.disposeActor();
        }



        //mainStage.dispose();
        //uiStage.dispose();



    }

    public void addListenersToButtons(){


        soundButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);


                playButtonSound();
                switchSound();
                playMainTheme();
            }


        });



        startButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);

                if(PlayerData.getFitstTime() == false){
                    PlayerData.firstTimeLounched(true);

                }


                playButtonSound();
                mainTheme.pause();
                dispose();

                game.setScreen(new LabirintMain(game));

            }


        });


        helpButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playButtonSound();
                setVisibilityOffForHelpButton();

            }


        });

        backButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playButtonSound();
                setVisibilityOnForHelpButton();

            }


        });

        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Gdx.app.log("BrickBreaker 2", "Button was pressed!");
                //pauseButton.background(resumeButtonTexture);
                playButtonSound();

                Gdx.app.exit();

            }


        });





    }


    public void playButtonSound(){
        if(BaseScreen.sounOn){
            buttonPress.play();
        }
    }

    public void playMainTheme(){
        if(BaseScreen.sounOn){
            mainTheme.play();
        }else{
            mainTheme.pause();
        }
    }


    @Override
    public void pause() {
        super.pause();
        if(mainTheme.isPlaying()){
            BaseScreen.sounOn =false;
            soundButton.toggle();
            mainTheme.pause();


        }
    }
}
