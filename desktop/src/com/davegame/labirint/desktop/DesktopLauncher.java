package com.davegame.labirint.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.davegame.labirint.LabirintMain;
import com.davegame.labirint.base.LevelLoader;

public class DesktopLauncher {
	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 1080;
		config.height = 1920;
		config.foregroundFPS =60;
		config.vSyncEnabled = true;
		new LwjglApplication(new LevelLoader(), config);
	}
}
